<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Model\LignefraishorsforfaitQuery;

class LigneFraisHorsForfaitController extends Controller {

    /**
     * Retourne les lignes de frais hors forfait d'une fiche de frais
     * 
     * @param type $id l'identifiant d'une fiche de frais
     * @return type les lignes de frais hors forfait d'une fiche de frais
     */
    public function show(Request $request, $id) {
        $collectionLigneFraisHorsForfait = LignefraishorsforfaitQuery::create()
                ->findByIdfichefrais($id);
        $collectionLigneFraisHorsForfait = $collectionLigneFraisHorsForfait->toJSON();
        return response($collectionLigneFraisHorsForfait);
    }
    
    /**
     * supprime une ligne de frais hors forfait
     * 
     * @param type $id l'identifiant d'une ligne de frais hors forfait 
     * @return JsonString la confirmation de la suppression
     */
    public function destroy(Request $request, $id) {
        $ligneFraisHorsForfait = LignefraishorsforfaitQuery::create()
                ->findByIdfichefrais($id);
        $ligneFraisHorsForfait->delete();
        return response(
                "{suppression effectuée}"
                );
    }

}
