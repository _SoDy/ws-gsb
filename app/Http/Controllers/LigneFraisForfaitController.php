<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Model\Base\LignefraisforfaitQuery;

class LigneFraisForfaitController extends Controller {

    /**
     * Retourne l'ensemble des lignes de frais forfait d'une fiche de frais
     * pour un visiteur authentifié
     * 
     * @return JsonString Collection de lignes de frais forfait
     */
    public function show(Request $request, $id) {
        $user = $request->session()->get('user');

        $collectionLigneFraisForfait = LignefraisforfaitQuery::create()
                ->findByIdfichefrais($id);

        $listeFicheFrais = $collectionLigneFraisForfait->toJSON();
        return response($listeFicheFrais);
    }

    /**
     * Modifie une ligne de frais forfait 
     * 
     *  
     * @param QueryParameter $id le numéro (ou identififiant) d'une fiche de frais
     * @return JsonString La ligne de fiche de frais modifié
     */
    public function update(Request $request, $id) {

        $user = $request->session()->get('user');
        $idFraisForfait = $request->post("idFraisForfait");
        $ligneFraisForfait = LignefraisforfaitQuery::create()
                ->findOneByArray(
                array(
                    'idFraisForfait' => $idFraisForfait,
                    'idFicheFrais' => $id
                )
        );
        $quantite = $request->post("quantite");

        $ligneFraisForfait->setQuantite($quantite);
        $ligneFraisForfait->save();

        return response($ligneFraisForfait->toJSON());
    }

}
