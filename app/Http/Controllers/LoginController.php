<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Model\UtilisateurQuery;

/**
 * Description of LoginController
 *
 * @author sody
 */
class LoginController extends Controller {

    public function authenticate(Request $request) {
        $resultat = false;
        $login = $request->post('login');
        $mdp = $request->post('mdp');

        $utilisateur = UtilisateurQuery::create()->findOneByArray(array(
            'login' => $login,
            'mdp' => $mdp,
            ));

        if ($utilisateur != null) {
            $request->session()->put('user', $utilisateur);
            $resultat = true;
        }
        return response()->json(array(
                    'connected' => $resultat
        ));
    }

}
