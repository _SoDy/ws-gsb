<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Model\EtatQuery;
use App\Http\Model\Etat;

class EtatController extends Controller {

    /**
     * Retourne l'ensemble des types d'état
     * 
     * @tutorial package
     * @return JsonModel Les types d'état au format JSON
     */
    public function index() {
        $collectionEtat = EtatQuery::create()
                ->find();
        $resultat = ($collectionEtat != null) ? $collectionEtat->toJSON() : null;
        return response($resultat);
    }

    /**
     * Retourne un état à partir d'un identifiant d'état transmis en paramètre
     * 
     * @param type $id
     * @return JsonModel Un état au format JSON
     */
    public function show($id) {
        $etat = EtatQuery::create()
                ->findOneByIdetat($id);
        $resultat = ($etat != null) ? $etat->toJSON() : null;
        return response($resultat);

    }

}
