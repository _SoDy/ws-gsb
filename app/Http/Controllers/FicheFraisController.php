<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Model\FichefraisQuery;
use App\Http\Model\Fichefrais;

class FicheFraisController extends Controller {

    /**
     * Retourne l'ensemble des des fiches de frais de l'utilisateur authentifié
     * 
     * @remark Les fiches de frais incluent les lignes de frais forfaitaires et hors forfait
     * @return JsonString Collection de fiches de frais
     */
    public function index(Request $request) {
        $user = $request->session()->get('user');

        $listeFicheFrais = FichefraisQuery::create()
                ->LeftJoinWithLignefraisforfait()
                ->leftJoinWithLignefraishorsforfait()
                ->findByIdvisiteur($user->getIdUser());

        $listeFicheFrais = $listeFicheFrais->toJSON();
        return response($listeFicheFrais);
    }

    /**
     * Modifie une fiche de frais à partir des données transmises en paramètres
     * 
     *  
     * @param QueryParameter $id le numéro (ou identififiant) d'une fiche de frais
     * @remarks Profil Comptable uniquement ; L'identifiant du Visiteur est obligatoire ; Seules les données transmises entrainent une mise à jour des champs concernés
     * @return JsonString La fiche de frais modifiée 
     */
    public function update(Request $request, $id) {

        $user = $request->session()->get('user');
        $profil = $user->getProfil();
        if ($profil == 'Visiteur') {
            $resultat = '{[Modifications Impossibles]}';
        } else {
            $ficheFrais = FichefraisQuery::create()
                    ->findOneByArray(
                    array(
                        "idVisiteur" => $request->post("idVisiteur"),
                        "idFicheFrais" => $id)
            );
            $nbJustificatifs = $request->post("nbJustificatifs");
            $montantValide = $request->post("montantValide");
            $idEtat = $request->post("idEtat");
            $ficheFrais->setDatemodif(time());
            $ficheFrais->setNbjustificatifs($nbJustificatifs);
            $ficheFrais->setMontantvalide($montantValide);
            $ficheFrais->setIdetat($idEtat);
            $ficheFrais->save();
            $resultat = $ficheFrais->toJSON();
        }

        return response($resultat);
    }

    /**
     * Retourne une fiche de frais à partir d'un identifiant de fiche de frais
     * 
     * @remark Les fiches de frais incluent les lignes de frais forfaitaires et hors forfait
     * 
     * @param QueryParameter $id l'identifiant de la fiche de frais
     * @return JsonString Une fiche de frais
     */
    public function show(Request $request, $id) {

        $user = $request->session()->get('user');

        $ficheFrais = FichefraisQuery::create()
                ->LeftJoinWithLignefraisforfait()
                ->leftJoinWithLignefraishorsforfait()
                ->filterByIdfichefrais($id)
                ->findByIdvisiteur($user->getIdUser());

        $ficheFrais = $ficheFrais->toJSON();


        return response($ficheFrais);
    }

    /**
     * retourne la fiche de frais courante et la créée si nécessaire.
     * 
     * @return JsonString La fiche de frais créée 
     */
    public function store(Request $request) {

        $user = $request->session()->get('user');
        $moisAnnee = $request->post("moisAnnee");

        $ff = Fichefrais::exist($user->getIdUser(), $moisAnnee);

        if (!$ff) {
            $ficheFrais = new Fichefrais();
            $ficheFrais->setMoisannee($moisAnnee);
            $ficheFrais->setIdvisiteur($user->getIdUser());
            $ficheFrais->setIdetat("EC");
            $ficheFrais->save();
        }

        $ficheFrais = FichefraisQuery::create()
                ->LeftJoinWithLignefraisforfait()
                ->leftJoinWithLignefraishorsforfait()
                ->findByArray(
                        array(
                            "idVisiteur" => $user->getIdUser(),
                            "moisAnnee" => $moisAnnee,
                ));
                
                
        $recapitulatif = $ficheFrais->toJSON();

        return response($recapitulatif);
    }
    
    public function destroy(Request $request, $id) {

        $resultat = false;
        $user = $request->session()->get('user');

        $ficheFrais = FichefraisQuery::create()
                ->filterByIdfichefrais($id)
                ->findByIdvisiteur($user->getIdUser());

        if ($ficheFrais != null) {
            $ficheFrais->delete();
            $resultat = true;
        }
        return response()->json(array(
                    'isDeleted' => $resultat,
        ));
    }
    
    

}
