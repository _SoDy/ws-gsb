<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Model\FraisforfaitQuery;

class FraisForfaitController extends Controller
{
    /**
     * retourne l'ensemble des frais forfaitaires
     * 
     * @return JsonString les frais forfaitaires au format JSON
     */
    public function index() {
        
         $collectionFraisForfait = 
                FraisforfaitQuery::create()
                ->find();
          return response($collectionFraisForfait->toJSON());    
        
    }
    
}
