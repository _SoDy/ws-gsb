<?php

namespace App\Http\Model\Map;

use App\Http\Model\Utilisateur;
use App\Http\Model\UtilisateurQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'utilisateur' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 */
class UtilisateurTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    public const CLASS_NAME = '.Map.UtilisateurTableMap';

    /**
     * The default database name for this class
     */
    public const DATABASE_NAME = 'ws-gsb-connection';

    /**
     * The table name for this class
     */
    public const TABLE_NAME = 'utilisateur';

    /**
     * The PHP name of this class (PascalCase)
     */
    public const TABLE_PHP_NAME = 'Utilisateur';

    /**
     * The related Propel class for this table
     */
    public const OM_CLASS = '\\App\\Http\\Model\\Utilisateur';

    /**
     * A class that can be returned by this tableMap
     */
    public const CLASS_DEFAULT = 'Utilisateur';

    /**
     * The total number of columns
     */
    public const NUM_COLUMNS = 10;

    /**
     * The number of lazy-loaded columns
     */
    public const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    public const NUM_HYDRATE_COLUMNS = 10;

    /**
     * the column name for the idUser field
     */
    public const COL_IDUSER = 'utilisateur.idUser';

    /**
     * the column name for the nom field
     */
    public const COL_NOM = 'utilisateur.nom';

    /**
     * the column name for the prenom field
     */
    public const COL_PRENOM = 'utilisateur.prenom';

    /**
     * the column name for the login field
     */
    public const COL_LOGIN = 'utilisateur.login';

    /**
     * the column name for the mdp field
     */
    public const COL_MDP = 'utilisateur.mdp';

    /**
     * the column name for the adresse field
     */
    public const COL_ADRESSE = 'utilisateur.adresse';

    /**
     * the column name for the ville field
     */
    public const COL_VILLE = 'utilisateur.ville';

    /**
     * the column name for the cp field
     */
    public const COL_CP = 'utilisateur.cp';

    /**
     * the column name for the dateEmbauche field
     */
    public const COL_DATEEMBAUCHE = 'utilisateur.dateEmbauche';

    /**
     * the column name for the profil field
     */
    public const COL_PROFIL = 'utilisateur.profil';

    /**
     * The default string format for model objects of the related table
     */
    public const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     *
     * @var array<string, mixed>
     */
    protected static $fieldNames = [
        self::TYPE_PHPNAME       => ['Iduser', 'Nom', 'Prenom', 'Login', 'Mdp', 'Adresse', 'Ville', 'Cp', 'Dateembauche', 'Profil', ],
        self::TYPE_CAMELNAME     => ['iduser', 'nom', 'prenom', 'login', 'mdp', 'adresse', 'ville', 'cp', 'dateembauche', 'profil', ],
        self::TYPE_COLNAME       => [UtilisateurTableMap::COL_IDUSER, UtilisateurTableMap::COL_NOM, UtilisateurTableMap::COL_PRENOM, UtilisateurTableMap::COL_LOGIN, UtilisateurTableMap::COL_MDP, UtilisateurTableMap::COL_ADRESSE, UtilisateurTableMap::COL_VILLE, UtilisateurTableMap::COL_CP, UtilisateurTableMap::COL_DATEEMBAUCHE, UtilisateurTableMap::COL_PROFIL, ],
        self::TYPE_FIELDNAME     => ['idUser', 'nom', 'prenom', 'login', 'mdp', 'adresse', 'ville', 'cp', 'dateEmbauche', 'profil', ],
        self::TYPE_NUM           => [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, ]
    ];

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     *
     * @var array<string, mixed>
     */
    protected static $fieldKeys = [
        self::TYPE_PHPNAME       => ['Iduser' => 0, 'Nom' => 1, 'Prenom' => 2, 'Login' => 3, 'Mdp' => 4, 'Adresse' => 5, 'Ville' => 6, 'Cp' => 7, 'Dateembauche' => 8, 'Profil' => 9, ],
        self::TYPE_CAMELNAME     => ['iduser' => 0, 'nom' => 1, 'prenom' => 2, 'login' => 3, 'mdp' => 4, 'adresse' => 5, 'ville' => 6, 'cp' => 7, 'dateembauche' => 8, 'profil' => 9, ],
        self::TYPE_COLNAME       => [UtilisateurTableMap::COL_IDUSER => 0, UtilisateurTableMap::COL_NOM => 1, UtilisateurTableMap::COL_PRENOM => 2, UtilisateurTableMap::COL_LOGIN => 3, UtilisateurTableMap::COL_MDP => 4, UtilisateurTableMap::COL_ADRESSE => 5, UtilisateurTableMap::COL_VILLE => 6, UtilisateurTableMap::COL_CP => 7, UtilisateurTableMap::COL_DATEEMBAUCHE => 8, UtilisateurTableMap::COL_PROFIL => 9, ],
        self::TYPE_FIELDNAME     => ['idUser' => 0, 'nom' => 1, 'prenom' => 2, 'login' => 3, 'mdp' => 4, 'adresse' => 5, 'ville' => 6, 'cp' => 7, 'dateEmbauche' => 8, 'profil' => 9, ],
        self::TYPE_NUM           => [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, ]
    ];

    /**
     * Holds a list of column names and their normalized version.
     *
     * @var array<string>
     */
    protected $normalizedColumnNameMap = [
        'Iduser' => 'IDUSER',
        'Utilisateur.Iduser' => 'IDUSER',
        'iduser' => 'IDUSER',
        'utilisateur.iduser' => 'IDUSER',
        'UtilisateurTableMap::COL_IDUSER' => 'IDUSER',
        'COL_IDUSER' => 'IDUSER',
        'idUser' => 'IDUSER',
        'utilisateur.idUser' => 'IDUSER',
        'Nom' => 'NOM',
        'Utilisateur.Nom' => 'NOM',
        'nom' => 'NOM',
        'utilisateur.nom' => 'NOM',
        'UtilisateurTableMap::COL_NOM' => 'NOM',
        'COL_NOM' => 'NOM',
        'Prenom' => 'PRENOM',
        'Utilisateur.Prenom' => 'PRENOM',
        'prenom' => 'PRENOM',
        'utilisateur.prenom' => 'PRENOM',
        'UtilisateurTableMap::COL_PRENOM' => 'PRENOM',
        'COL_PRENOM' => 'PRENOM',
        'Login' => 'LOGIN',
        'Utilisateur.Login' => 'LOGIN',
        'login' => 'LOGIN',
        'utilisateur.login' => 'LOGIN',
        'UtilisateurTableMap::COL_LOGIN' => 'LOGIN',
        'COL_LOGIN' => 'LOGIN',
        'Mdp' => 'MDP',
        'Utilisateur.Mdp' => 'MDP',
        'mdp' => 'MDP',
        'utilisateur.mdp' => 'MDP',
        'UtilisateurTableMap::COL_MDP' => 'MDP',
        'COL_MDP' => 'MDP',
        'Adresse' => 'ADRESSE',
        'Utilisateur.Adresse' => 'ADRESSE',
        'adresse' => 'ADRESSE',
        'utilisateur.adresse' => 'ADRESSE',
        'UtilisateurTableMap::COL_ADRESSE' => 'ADRESSE',
        'COL_ADRESSE' => 'ADRESSE',
        'Ville' => 'VILLE',
        'Utilisateur.Ville' => 'VILLE',
        'ville' => 'VILLE',
        'utilisateur.ville' => 'VILLE',
        'UtilisateurTableMap::COL_VILLE' => 'VILLE',
        'COL_VILLE' => 'VILLE',
        'Cp' => 'CP',
        'Utilisateur.Cp' => 'CP',
        'cp' => 'CP',
        'utilisateur.cp' => 'CP',
        'UtilisateurTableMap::COL_CP' => 'CP',
        'COL_CP' => 'CP',
        'Dateembauche' => 'DATEEMBAUCHE',
        'Utilisateur.Dateembauche' => 'DATEEMBAUCHE',
        'dateembauche' => 'DATEEMBAUCHE',
        'utilisateur.dateembauche' => 'DATEEMBAUCHE',
        'UtilisateurTableMap::COL_DATEEMBAUCHE' => 'DATEEMBAUCHE',
        'COL_DATEEMBAUCHE' => 'DATEEMBAUCHE',
        'dateEmbauche' => 'DATEEMBAUCHE',
        'utilisateur.dateEmbauche' => 'DATEEMBAUCHE',
        'Profil' => 'PROFIL',
        'Utilisateur.Profil' => 'PROFIL',
        'profil' => 'PROFIL',
        'utilisateur.profil' => 'PROFIL',
        'UtilisateurTableMap::COL_PROFIL' => 'PROFIL',
        'COL_PROFIL' => 'PROFIL',
    ];

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function initialize(): void
    {
        // attributes
        $this->setName('utilisateur');
        $this->setPhpName('Utilisateur');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\App\\Http\\Model\\Utilisateur');
        $this->setPackage('');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('idUser', 'Iduser', 'VARCHAR', true, 4, null);
        $this->addColumn('nom', 'Nom', 'VARCHAR', true, 30, null);
        $this->addColumn('prenom', 'Prenom', 'VARCHAR', true, 30, null);
        $this->addColumn('login', 'Login', 'VARCHAR', true, 20, null);
        $this->addColumn('mdp', 'Mdp', 'VARCHAR', true, 20, null);
        $this->addColumn('adresse', 'Adresse', 'VARCHAR', true, 30, null);
        $this->addColumn('ville', 'Ville', 'VARCHAR', true, 30, null);
        $this->addColumn('cp', 'Cp', 'VARCHAR', true, 5, null);
        $this->addColumn('dateEmbauche', 'Dateembauche', 'DATE', true, null, null);
        $this->addColumn('profil', 'Profil', 'CHAR', true, null, null);
    }

    /**
     * Build the RelationMap objects for this table relationships
     *
     * @return void
     */
    public function buildRelations(): void
    {
        $this->addRelation('Fichefrais', '\\App\\Http\\Model\\Fichefrais', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':idVisiteur',
    1 => ':idUser',
  ),
), null, null, 'Fichefraiss', false);
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array $row Resultset row.
     * @param int $offset The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string|null The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow(array $row, int $offset = 0, string $indexType = TableMap::TYPE_NUM): ?string
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Iduser', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Iduser', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Iduser', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Iduser', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Iduser', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Iduser', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array $row Resultset row.
     * @param int $offset The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow(array $row, int $offset = 0, string $indexType = TableMap::TYPE_NUM)
    {
        return (string) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Iduser', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param bool $withPrefix Whether to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass(bool $withPrefix = true): string
    {
        return $withPrefix ? UtilisateurTableMap::CLASS_DEFAULT : UtilisateurTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array $row Row returned by DataFetcher->fetch().
     * @param int $offset The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array (Utilisateur object, last column rank)
     */
    public static function populateObject(array $row, int $offset = 0, string $indexType = TableMap::TYPE_NUM): array
    {
        $key = UtilisateurTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = UtilisateurTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + UtilisateurTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = UtilisateurTableMap::OM_CLASS;
            /** @var Utilisateur $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            UtilisateurTableMap::addInstanceToPool($obj, $key);
        }

        return [$obj, $col];
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array<object>
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher): array
    {
        $results = [];

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = UtilisateurTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = UtilisateurTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Utilisateur $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                UtilisateurTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria Object containing the columns to add.
     * @param string|null $alias Optional table alias
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return void
     */
    public static function addSelectColumns(Criteria $criteria, ?string $alias = null): void
    {
        if (null === $alias) {
            $criteria->addSelectColumn(UtilisateurTableMap::COL_IDUSER);
            $criteria->addSelectColumn(UtilisateurTableMap::COL_NOM);
            $criteria->addSelectColumn(UtilisateurTableMap::COL_PRENOM);
            $criteria->addSelectColumn(UtilisateurTableMap::COL_LOGIN);
            $criteria->addSelectColumn(UtilisateurTableMap::COL_MDP);
            $criteria->addSelectColumn(UtilisateurTableMap::COL_ADRESSE);
            $criteria->addSelectColumn(UtilisateurTableMap::COL_VILLE);
            $criteria->addSelectColumn(UtilisateurTableMap::COL_CP);
            $criteria->addSelectColumn(UtilisateurTableMap::COL_DATEEMBAUCHE);
            $criteria->addSelectColumn(UtilisateurTableMap::COL_PROFIL);
        } else {
            $criteria->addSelectColumn($alias . '.idUser');
            $criteria->addSelectColumn($alias . '.nom');
            $criteria->addSelectColumn($alias . '.prenom');
            $criteria->addSelectColumn($alias . '.login');
            $criteria->addSelectColumn($alias . '.mdp');
            $criteria->addSelectColumn($alias . '.adresse');
            $criteria->addSelectColumn($alias . '.ville');
            $criteria->addSelectColumn($alias . '.cp');
            $criteria->addSelectColumn($alias . '.dateEmbauche');
            $criteria->addSelectColumn($alias . '.profil');
        }
    }

    /**
     * Remove all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be removed as they are only loaded on demand.
     *
     * @param Criteria $criteria Object containing the columns to remove.
     * @param string|null $alias Optional table alias
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return void
     */
    public static function removeSelectColumns(Criteria $criteria, ?string $alias = null): void
    {
        if (null === $alias) {
            $criteria->removeSelectColumn(UtilisateurTableMap::COL_IDUSER);
            $criteria->removeSelectColumn(UtilisateurTableMap::COL_NOM);
            $criteria->removeSelectColumn(UtilisateurTableMap::COL_PRENOM);
            $criteria->removeSelectColumn(UtilisateurTableMap::COL_LOGIN);
            $criteria->removeSelectColumn(UtilisateurTableMap::COL_MDP);
            $criteria->removeSelectColumn(UtilisateurTableMap::COL_ADRESSE);
            $criteria->removeSelectColumn(UtilisateurTableMap::COL_VILLE);
            $criteria->removeSelectColumn(UtilisateurTableMap::COL_CP);
            $criteria->removeSelectColumn(UtilisateurTableMap::COL_DATEEMBAUCHE);
            $criteria->removeSelectColumn(UtilisateurTableMap::COL_PROFIL);
        } else {
            $criteria->removeSelectColumn($alias . '.idUser');
            $criteria->removeSelectColumn($alias . '.nom');
            $criteria->removeSelectColumn($alias . '.prenom');
            $criteria->removeSelectColumn($alias . '.login');
            $criteria->removeSelectColumn($alias . '.mdp');
            $criteria->removeSelectColumn($alias . '.adresse');
            $criteria->removeSelectColumn($alias . '.ville');
            $criteria->removeSelectColumn($alias . '.cp');
            $criteria->removeSelectColumn($alias . '.dateEmbauche');
            $criteria->removeSelectColumn($alias . '.profil');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap(): TableMap
    {
        return Propel::getServiceContainer()->getDatabaseMap(UtilisateurTableMap::DATABASE_NAME)->getTable(UtilisateurTableMap::TABLE_NAME);
    }

    /**
     * Performs a DELETE on the database, given a Utilisateur or Criteria object OR a primary key value.
     *
     * @param mixed $values Criteria or Utilisateur object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ?ConnectionInterface $con = null): int
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UtilisateurTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \App\Http\Model\Utilisateur) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(UtilisateurTableMap::DATABASE_NAME);
            $criteria->add(UtilisateurTableMap::COL_IDUSER, (array) $values, Criteria::IN);
        }

        $query = UtilisateurQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            UtilisateurTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                UtilisateurTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the utilisateur table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(?ConnectionInterface $con = null): int
    {
        return UtilisateurQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Utilisateur or Criteria object.
     *
     * @param mixed $criteria Criteria or Utilisateur object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed The new primary key.
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ?ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UtilisateurTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Utilisateur object
        }


        // Set the correct dbName
        $query = UtilisateurQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

}
