<?php

namespace App\Http\Model\Map;

use App\Http\Model\Lignefraishorsforfait;
use App\Http\Model\LignefraishorsforfaitQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'lignefraishorsforfait' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 */
class LignefraishorsforfaitTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    public const CLASS_NAME = '.Map.LignefraishorsforfaitTableMap';

    /**
     * The default database name for this class
     */
    public const DATABASE_NAME = 'ws-gsb-connection';

    /**
     * The table name for this class
     */
    public const TABLE_NAME = 'lignefraishorsforfait';

    /**
     * The PHP name of this class (PascalCase)
     */
    public const TABLE_PHP_NAME = 'Lignefraishorsforfait';

    /**
     * The related Propel class for this table
     */
    public const OM_CLASS = '\\App\\Http\\Model\\Lignefraishorsforfait';

    /**
     * A class that can be returned by this tableMap
     */
    public const CLASS_DEFAULT = 'Lignefraishorsforfait';

    /**
     * The total number of columns
     */
    public const NUM_COLUMNS = 7;

    /**
     * The number of lazy-loaded columns
     */
    public const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    public const NUM_HYDRATE_COLUMNS = 7;

    /**
     * the column name for the idFicheFrais field
     */
    public const COL_IDFICHEFRAIS = 'lignefraishorsforfait.idFicheFrais';

    /**
     * the column name for the idLigneFraisHorsForfait field
     */
    public const COL_IDLIGNEFRAISHORSFORFAIT = 'lignefraishorsforfait.idLigneFraisHorsForfait';

    /**
     * the column name for the date field
     */
    public const COL_DATE = 'lignefraishorsforfait.date';

    /**
     * the column name for the montant field
     */
    public const COL_MONTANT = 'lignefraishorsforfait.montant';

    /**
     * the column name for the libelle field
     */
    public const COL_LIBELLE = 'lignefraishorsforfait.libelle';

    /**
     * the column name for the justificatif field
     */
    public const COL_JUSTIFICATIF = 'lignefraishorsforfait.justificatif';

    /**
     * the column name for the nomJustificatif field
     */
    public const COL_NOMJUSTIFICATIF = 'lignefraishorsforfait.nomJustificatif';

    /**
     * The default string format for model objects of the related table
     */
    public const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     *
     * @var array<string, mixed>
     */
    protected static $fieldNames = [
        self::TYPE_PHPNAME       => ['Idfichefrais', 'Idlignefraishorsforfait', 'Date', 'Montant', 'Libelle', 'Justificatif', 'Nomjustificatif', ],
        self::TYPE_CAMELNAME     => ['idfichefrais', 'idlignefraishorsforfait', 'date', 'montant', 'libelle', 'justificatif', 'nomjustificatif', ],
        self::TYPE_COLNAME       => [LignefraishorsforfaitTableMap::COL_IDFICHEFRAIS, LignefraishorsforfaitTableMap::COL_IDLIGNEFRAISHORSFORFAIT, LignefraishorsforfaitTableMap::COL_DATE, LignefraishorsforfaitTableMap::COL_MONTANT, LignefraishorsforfaitTableMap::COL_LIBELLE, LignefraishorsforfaitTableMap::COL_JUSTIFICATIF, LignefraishorsforfaitTableMap::COL_NOMJUSTIFICATIF, ],
        self::TYPE_FIELDNAME     => ['idFicheFrais', 'idLigneFraisHorsForfait', 'date', 'montant', 'libelle', 'justificatif', 'nomJustificatif', ],
        self::TYPE_NUM           => [0, 1, 2, 3, 4, 5, 6, ]
    ];

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     *
     * @var array<string, mixed>
     */
    protected static $fieldKeys = [
        self::TYPE_PHPNAME       => ['Idfichefrais' => 0, 'Idlignefraishorsforfait' => 1, 'Date' => 2, 'Montant' => 3, 'Libelle' => 4, 'Justificatif' => 5, 'Nomjustificatif' => 6, ],
        self::TYPE_CAMELNAME     => ['idfichefrais' => 0, 'idlignefraishorsforfait' => 1, 'date' => 2, 'montant' => 3, 'libelle' => 4, 'justificatif' => 5, 'nomjustificatif' => 6, ],
        self::TYPE_COLNAME       => [LignefraishorsforfaitTableMap::COL_IDFICHEFRAIS => 0, LignefraishorsforfaitTableMap::COL_IDLIGNEFRAISHORSFORFAIT => 1, LignefraishorsforfaitTableMap::COL_DATE => 2, LignefraishorsforfaitTableMap::COL_MONTANT => 3, LignefraishorsforfaitTableMap::COL_LIBELLE => 4, LignefraishorsforfaitTableMap::COL_JUSTIFICATIF => 5, LignefraishorsforfaitTableMap::COL_NOMJUSTIFICATIF => 6, ],
        self::TYPE_FIELDNAME     => ['idFicheFrais' => 0, 'idLigneFraisHorsForfait' => 1, 'date' => 2, 'montant' => 3, 'libelle' => 4, 'justificatif' => 5, 'nomJustificatif' => 6, ],
        self::TYPE_NUM           => [0, 1, 2, 3, 4, 5, 6, ]
    ];

    /**
     * Holds a list of column names and their normalized version.
     *
     * @var array<string>
     */
    protected $normalizedColumnNameMap = [
        'Idfichefrais' => 'IDFICHEFRAIS',
        'Lignefraishorsforfait.Idfichefrais' => 'IDFICHEFRAIS',
        'idfichefrais' => 'IDFICHEFRAIS',
        'lignefraishorsforfait.idfichefrais' => 'IDFICHEFRAIS',
        'LignefraishorsforfaitTableMap::COL_IDFICHEFRAIS' => 'IDFICHEFRAIS',
        'COL_IDFICHEFRAIS' => 'IDFICHEFRAIS',
        'idFicheFrais' => 'IDFICHEFRAIS',
        'lignefraishorsforfait.idFicheFrais' => 'IDFICHEFRAIS',
        'Idlignefraishorsforfait' => 'IDLIGNEFRAISHORSFORFAIT',
        'Lignefraishorsforfait.Idlignefraishorsforfait' => 'IDLIGNEFRAISHORSFORFAIT',
        'idlignefraishorsforfait' => 'IDLIGNEFRAISHORSFORFAIT',
        'lignefraishorsforfait.idlignefraishorsforfait' => 'IDLIGNEFRAISHORSFORFAIT',
        'LignefraishorsforfaitTableMap::COL_IDLIGNEFRAISHORSFORFAIT' => 'IDLIGNEFRAISHORSFORFAIT',
        'COL_IDLIGNEFRAISHORSFORFAIT' => 'IDLIGNEFRAISHORSFORFAIT',
        'idLigneFraisHorsForfait' => 'IDLIGNEFRAISHORSFORFAIT',
        'lignefraishorsforfait.idLigneFraisHorsForfait' => 'IDLIGNEFRAISHORSFORFAIT',
        'Date' => 'DATE',
        'Lignefraishorsforfait.Date' => 'DATE',
        'date' => 'DATE',
        'lignefraishorsforfait.date' => 'DATE',
        'LignefraishorsforfaitTableMap::COL_DATE' => 'DATE',
        'COL_DATE' => 'DATE',
        'Montant' => 'MONTANT',
        'Lignefraishorsforfait.Montant' => 'MONTANT',
        'montant' => 'MONTANT',
        'lignefraishorsforfait.montant' => 'MONTANT',
        'LignefraishorsforfaitTableMap::COL_MONTANT' => 'MONTANT',
        'COL_MONTANT' => 'MONTANT',
        'Libelle' => 'LIBELLE',
        'Lignefraishorsforfait.Libelle' => 'LIBELLE',
        'libelle' => 'LIBELLE',
        'lignefraishorsforfait.libelle' => 'LIBELLE',
        'LignefraishorsforfaitTableMap::COL_LIBELLE' => 'LIBELLE',
        'COL_LIBELLE' => 'LIBELLE',
        'Justificatif' => 'JUSTIFICATIF',
        'Lignefraishorsforfait.Justificatif' => 'JUSTIFICATIF',
        'justificatif' => 'JUSTIFICATIF',
        'lignefraishorsforfait.justificatif' => 'JUSTIFICATIF',
        'LignefraishorsforfaitTableMap::COL_JUSTIFICATIF' => 'JUSTIFICATIF',
        'COL_JUSTIFICATIF' => 'JUSTIFICATIF',
        'Nomjustificatif' => 'NOMJUSTIFICATIF',
        'Lignefraishorsforfait.Nomjustificatif' => 'NOMJUSTIFICATIF',
        'nomjustificatif' => 'NOMJUSTIFICATIF',
        'lignefraishorsforfait.nomjustificatif' => 'NOMJUSTIFICATIF',
        'LignefraishorsforfaitTableMap::COL_NOMJUSTIFICATIF' => 'NOMJUSTIFICATIF',
        'COL_NOMJUSTIFICATIF' => 'NOMJUSTIFICATIF',
        'nomJustificatif' => 'NOMJUSTIFICATIF',
        'lignefraishorsforfait.nomJustificatif' => 'NOMJUSTIFICATIF',
    ];

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function initialize(): void
    {
        // attributes
        $this->setName('lignefraishorsforfait');
        $this->setPhpName('Lignefraishorsforfait');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\App\\Http\\Model\\Lignefraishorsforfait');
        $this->setPackage('');
        $this->setUseIdGenerator(true);
        // columns
        $this->addColumn('idFicheFrais', 'Idfichefrais', 'INTEGER', true, null, null);
        $this->addPrimaryKey('idLigneFraisHorsForfait', 'Idlignefraishorsforfait', 'INTEGER', true, null, null);
        $this->addColumn('date', 'Date', 'INTEGER', true, null, null);
        $this->addColumn('montant', 'Montant', 'DECIMAL', true, 10, null);
        $this->addColumn('libelle', 'Libelle', 'VARCHAR', true, 100, null);
        $this->addColumn('justificatif', 'Justificatif', 'BOOLEAN', true, 1, false);
        $this->addColumn('nomJustificatif', 'Nomjustificatif', 'VARCHAR', false, 30, null);
    }

    /**
     * Build the RelationMap objects for this table relationships
     *
     * @return void
     */
    public function buildRelations(): void
    {
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array $row Resultset row.
     * @param int $offset The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string|null The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow(array $row, int $offset = 0, string $indexType = TableMap::TYPE_NUM): ?string
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 1 + $offset : static::translateFieldName('Idlignefraishorsforfait', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 1 + $offset : static::translateFieldName('Idlignefraishorsforfait', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 1 + $offset : static::translateFieldName('Idlignefraishorsforfait', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 1 + $offset : static::translateFieldName('Idlignefraishorsforfait', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 1 + $offset : static::translateFieldName('Idlignefraishorsforfait', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 1 + $offset : static::translateFieldName('Idlignefraishorsforfait', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array $row Resultset row.
     * @param int $offset The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow(array $row, int $offset = 0, string $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 1 + $offset
                : self::translateFieldName('Idlignefraishorsforfait', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param bool $withPrefix Whether to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass(bool $withPrefix = true): string
    {
        return $withPrefix ? LignefraishorsforfaitTableMap::CLASS_DEFAULT : LignefraishorsforfaitTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array $row Row returned by DataFetcher->fetch().
     * @param int $offset The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array (Lignefraishorsforfait object, last column rank)
     */
    public static function populateObject(array $row, int $offset = 0, string $indexType = TableMap::TYPE_NUM): array
    {
        $key = LignefraishorsforfaitTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = LignefraishorsforfaitTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + LignefraishorsforfaitTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = LignefraishorsforfaitTableMap::OM_CLASS;
            /** @var Lignefraishorsforfait $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            LignefraishorsforfaitTableMap::addInstanceToPool($obj, $key);
        }

        return [$obj, $col];
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array<object>
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher): array
    {
        $results = [];

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = LignefraishorsforfaitTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = LignefraishorsforfaitTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Lignefraishorsforfait $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                LignefraishorsforfaitTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria Object containing the columns to add.
     * @param string|null $alias Optional table alias
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return void
     */
    public static function addSelectColumns(Criteria $criteria, ?string $alias = null): void
    {
        if (null === $alias) {
            $criteria->addSelectColumn(LignefraishorsforfaitTableMap::COL_IDFICHEFRAIS);
            $criteria->addSelectColumn(LignefraishorsforfaitTableMap::COL_IDLIGNEFRAISHORSFORFAIT);
            $criteria->addSelectColumn(LignefraishorsforfaitTableMap::COL_DATE);
            $criteria->addSelectColumn(LignefraishorsforfaitTableMap::COL_MONTANT);
            $criteria->addSelectColumn(LignefraishorsforfaitTableMap::COL_LIBELLE);
            $criteria->addSelectColumn(LignefraishorsforfaitTableMap::COL_JUSTIFICATIF);
            $criteria->addSelectColumn(LignefraishorsforfaitTableMap::COL_NOMJUSTIFICATIF);
        } else {
            $criteria->addSelectColumn($alias . '.idFicheFrais');
            $criteria->addSelectColumn($alias . '.idLigneFraisHorsForfait');
            $criteria->addSelectColumn($alias . '.date');
            $criteria->addSelectColumn($alias . '.montant');
            $criteria->addSelectColumn($alias . '.libelle');
            $criteria->addSelectColumn($alias . '.justificatif');
            $criteria->addSelectColumn($alias . '.nomJustificatif');
        }
    }

    /**
     * Remove all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be removed as they are only loaded on demand.
     *
     * @param Criteria $criteria Object containing the columns to remove.
     * @param string|null $alias Optional table alias
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return void
     */
    public static function removeSelectColumns(Criteria $criteria, ?string $alias = null): void
    {
        if (null === $alias) {
            $criteria->removeSelectColumn(LignefraishorsforfaitTableMap::COL_IDFICHEFRAIS);
            $criteria->removeSelectColumn(LignefraishorsforfaitTableMap::COL_IDLIGNEFRAISHORSFORFAIT);
            $criteria->removeSelectColumn(LignefraishorsforfaitTableMap::COL_DATE);
            $criteria->removeSelectColumn(LignefraishorsforfaitTableMap::COL_MONTANT);
            $criteria->removeSelectColumn(LignefraishorsforfaitTableMap::COL_LIBELLE);
            $criteria->removeSelectColumn(LignefraishorsforfaitTableMap::COL_JUSTIFICATIF);
            $criteria->removeSelectColumn(LignefraishorsforfaitTableMap::COL_NOMJUSTIFICATIF);
        } else {
            $criteria->removeSelectColumn($alias . '.idFicheFrais');
            $criteria->removeSelectColumn($alias . '.idLigneFraisHorsForfait');
            $criteria->removeSelectColumn($alias . '.date');
            $criteria->removeSelectColumn($alias . '.montant');
            $criteria->removeSelectColumn($alias . '.libelle');
            $criteria->removeSelectColumn($alias . '.justificatif');
            $criteria->removeSelectColumn($alias . '.nomJustificatif');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap(): TableMap
    {
        return Propel::getServiceContainer()->getDatabaseMap(LignefraishorsforfaitTableMap::DATABASE_NAME)->getTable(LignefraishorsforfaitTableMap::TABLE_NAME);
    }

    /**
     * Performs a DELETE on the database, given a Lignefraishorsforfait or Criteria object OR a primary key value.
     *
     * @param mixed $values Criteria or Lignefraishorsforfait object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ?ConnectionInterface $con = null): int
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(LignefraishorsforfaitTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \App\Http\Model\Lignefraishorsforfait) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(LignefraishorsforfaitTableMap::DATABASE_NAME);
            $criteria->add(LignefraishorsforfaitTableMap::COL_IDLIGNEFRAISHORSFORFAIT, (array) $values, Criteria::IN);
        }

        $query = LignefraishorsforfaitQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            LignefraishorsforfaitTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                LignefraishorsforfaitTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the lignefraishorsforfait table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(?ConnectionInterface $con = null): int
    {
        return LignefraishorsforfaitQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Lignefraishorsforfait or Criteria object.
     *
     * @param mixed $criteria Criteria or Lignefraishorsforfait object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed The new primary key.
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ?ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(LignefraishorsforfaitTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Lignefraishorsforfait object
        }

        if ($criteria->containsKey(LignefraishorsforfaitTableMap::COL_IDLIGNEFRAISHORSFORFAIT) && $criteria->keyContainsValue(LignefraishorsforfaitTableMap::COL_IDLIGNEFRAISHORSFORFAIT) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.LignefraishorsforfaitTableMap::COL_IDLIGNEFRAISHORSFORFAIT.')');
        }


        // Set the correct dbName
        $query = LignefraishorsforfaitQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

}
