<?php

namespace App\Http\Model\Map;

use App\Http\Model\Fraisforfait;
use App\Http\Model\FraisforfaitQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'fraisforfait' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 */
class FraisforfaitTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    public const CLASS_NAME = '.Map.FraisforfaitTableMap';

    /**
     * The default database name for this class
     */
    public const DATABASE_NAME = 'ws-gsb-connection';

    /**
     * The table name for this class
     */
    public const TABLE_NAME = 'fraisforfait';

    /**
     * The PHP name of this class (PascalCase)
     */
    public const TABLE_PHP_NAME = 'Fraisforfait';

    /**
     * The related Propel class for this table
     */
    public const OM_CLASS = '\\App\\Http\\Model\\Fraisforfait';

    /**
     * A class that can be returned by this tableMap
     */
    public const CLASS_DEFAULT = 'Fraisforfait';

    /**
     * The total number of columns
     */
    public const NUM_COLUMNS = 3;

    /**
     * The number of lazy-loaded columns
     */
    public const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    public const NUM_HYDRATE_COLUMNS = 3;

    /**
     * the column name for the idFraisForfait field
     */
    public const COL_IDFRAISFORFAIT = 'fraisforfait.idFraisForfait';

    /**
     * the column name for the libelleFraisForfait field
     */
    public const COL_LIBELLEFRAISFORFAIT = 'fraisforfait.libelleFraisForfait';

    /**
     * the column name for the montantFraisForfait field
     */
    public const COL_MONTANTFRAISFORFAIT = 'fraisforfait.montantFraisForfait';

    /**
     * The default string format for model objects of the related table
     */
    public const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     *
     * @var array<string, mixed>
     */
    protected static $fieldNames = [
        self::TYPE_PHPNAME       => ['Idfraisforfait', 'Libellefraisforfait', 'Montantfraisforfait', ],
        self::TYPE_CAMELNAME     => ['idfraisforfait', 'libellefraisforfait', 'montantfraisforfait', ],
        self::TYPE_COLNAME       => [FraisforfaitTableMap::COL_IDFRAISFORFAIT, FraisforfaitTableMap::COL_LIBELLEFRAISFORFAIT, FraisforfaitTableMap::COL_MONTANTFRAISFORFAIT, ],
        self::TYPE_FIELDNAME     => ['idFraisForfait', 'libelleFraisForfait', 'montantFraisForfait', ],
        self::TYPE_NUM           => [0, 1, 2, ]
    ];

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     *
     * @var array<string, mixed>
     */
    protected static $fieldKeys = [
        self::TYPE_PHPNAME       => ['Idfraisforfait' => 0, 'Libellefraisforfait' => 1, 'Montantfraisforfait' => 2, ],
        self::TYPE_CAMELNAME     => ['idfraisforfait' => 0, 'libellefraisforfait' => 1, 'montantfraisforfait' => 2, ],
        self::TYPE_COLNAME       => [FraisforfaitTableMap::COL_IDFRAISFORFAIT => 0, FraisforfaitTableMap::COL_LIBELLEFRAISFORFAIT => 1, FraisforfaitTableMap::COL_MONTANTFRAISFORFAIT => 2, ],
        self::TYPE_FIELDNAME     => ['idFraisForfait' => 0, 'libelleFraisForfait' => 1, 'montantFraisForfait' => 2, ],
        self::TYPE_NUM           => [0, 1, 2, ]
    ];

    /**
     * Holds a list of column names and their normalized version.
     *
     * @var array<string>
     */
    protected $normalizedColumnNameMap = [
        'Idfraisforfait' => 'IDFRAISFORFAIT',
        'Fraisforfait.Idfraisforfait' => 'IDFRAISFORFAIT',
        'idfraisforfait' => 'IDFRAISFORFAIT',
        'fraisforfait.idfraisforfait' => 'IDFRAISFORFAIT',
        'FraisforfaitTableMap::COL_IDFRAISFORFAIT' => 'IDFRAISFORFAIT',
        'COL_IDFRAISFORFAIT' => 'IDFRAISFORFAIT',
        'idFraisForfait' => 'IDFRAISFORFAIT',
        'fraisforfait.idFraisForfait' => 'IDFRAISFORFAIT',
        'Libellefraisforfait' => 'LIBELLEFRAISFORFAIT',
        'Fraisforfait.Libellefraisforfait' => 'LIBELLEFRAISFORFAIT',
        'libellefraisforfait' => 'LIBELLEFRAISFORFAIT',
        'fraisforfait.libellefraisforfait' => 'LIBELLEFRAISFORFAIT',
        'FraisforfaitTableMap::COL_LIBELLEFRAISFORFAIT' => 'LIBELLEFRAISFORFAIT',
        'COL_LIBELLEFRAISFORFAIT' => 'LIBELLEFRAISFORFAIT',
        'libelleFraisForfait' => 'LIBELLEFRAISFORFAIT',
        'fraisforfait.libelleFraisForfait' => 'LIBELLEFRAISFORFAIT',
        'Montantfraisforfait' => 'MONTANTFRAISFORFAIT',
        'Fraisforfait.Montantfraisforfait' => 'MONTANTFRAISFORFAIT',
        'montantfraisforfait' => 'MONTANTFRAISFORFAIT',
        'fraisforfait.montantfraisforfait' => 'MONTANTFRAISFORFAIT',
        'FraisforfaitTableMap::COL_MONTANTFRAISFORFAIT' => 'MONTANTFRAISFORFAIT',
        'COL_MONTANTFRAISFORFAIT' => 'MONTANTFRAISFORFAIT',
        'montantFraisForfait' => 'MONTANTFRAISFORFAIT',
        'fraisforfait.montantFraisForfait' => 'MONTANTFRAISFORFAIT',
    ];

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function initialize(): void
    {
        // attributes
        $this->setName('fraisforfait');
        $this->setPhpName('Fraisforfait');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\App\\Http\\Model\\Fraisforfait');
        $this->setPackage('');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('idFraisForfait', 'Idfraisforfait', 'VARCHAR', true, 3, null);
        $this->addColumn('libelleFraisForfait', 'Libellefraisforfait', 'VARCHAR', true, 20, null);
        $this->addColumn('montantFraisForfait', 'Montantfraisforfait', 'DECIMAL', true, 5, null);
    }

    /**
     * Build the RelationMap objects for this table relationships
     *
     * @return void
     */
    public function buildRelations(): void
    {
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array $row Resultset row.
     * @param int $offset The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string|null The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow(array $row, int $offset = 0, string $indexType = TableMap::TYPE_NUM): ?string
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idfraisforfait', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idfraisforfait', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idfraisforfait', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idfraisforfait', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idfraisforfait', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idfraisforfait', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array $row Resultset row.
     * @param int $offset The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow(array $row, int $offset = 0, string $indexType = TableMap::TYPE_NUM)
    {
        return (string) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Idfraisforfait', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param bool $withPrefix Whether to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass(bool $withPrefix = true): string
    {
        return $withPrefix ? FraisforfaitTableMap::CLASS_DEFAULT : FraisforfaitTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array $row Row returned by DataFetcher->fetch().
     * @param int $offset The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array (Fraisforfait object, last column rank)
     */
    public static function populateObject(array $row, int $offset = 0, string $indexType = TableMap::TYPE_NUM): array
    {
        $key = FraisforfaitTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = FraisforfaitTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + FraisforfaitTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = FraisforfaitTableMap::OM_CLASS;
            /** @var Fraisforfait $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            FraisforfaitTableMap::addInstanceToPool($obj, $key);
        }

        return [$obj, $col];
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array<object>
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher): array
    {
        $results = [];

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = FraisforfaitTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = FraisforfaitTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Fraisforfait $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                FraisforfaitTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria Object containing the columns to add.
     * @param string|null $alias Optional table alias
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return void
     */
    public static function addSelectColumns(Criteria $criteria, ?string $alias = null): void
    {
        if (null === $alias) {
            $criteria->addSelectColumn(FraisforfaitTableMap::COL_IDFRAISFORFAIT);
            $criteria->addSelectColumn(FraisforfaitTableMap::COL_LIBELLEFRAISFORFAIT);
            $criteria->addSelectColumn(FraisforfaitTableMap::COL_MONTANTFRAISFORFAIT);
        } else {
            $criteria->addSelectColumn($alias . '.idFraisForfait');
            $criteria->addSelectColumn($alias . '.libelleFraisForfait');
            $criteria->addSelectColumn($alias . '.montantFraisForfait');
        }
    }

    /**
     * Remove all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be removed as they are only loaded on demand.
     *
     * @param Criteria $criteria Object containing the columns to remove.
     * @param string|null $alias Optional table alias
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return void
     */
    public static function removeSelectColumns(Criteria $criteria, ?string $alias = null): void
    {
        if (null === $alias) {
            $criteria->removeSelectColumn(FraisforfaitTableMap::COL_IDFRAISFORFAIT);
            $criteria->removeSelectColumn(FraisforfaitTableMap::COL_LIBELLEFRAISFORFAIT);
            $criteria->removeSelectColumn(FraisforfaitTableMap::COL_MONTANTFRAISFORFAIT);
        } else {
            $criteria->removeSelectColumn($alias . '.idFraisForfait');
            $criteria->removeSelectColumn($alias . '.libelleFraisForfait');
            $criteria->removeSelectColumn($alias . '.montantFraisForfait');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap(): TableMap
    {
        return Propel::getServiceContainer()->getDatabaseMap(FraisforfaitTableMap::DATABASE_NAME)->getTable(FraisforfaitTableMap::TABLE_NAME);
    }

    /**
     * Performs a DELETE on the database, given a Fraisforfait or Criteria object OR a primary key value.
     *
     * @param mixed $values Criteria or Fraisforfait object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ?ConnectionInterface $con = null): int
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(FraisforfaitTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \App\Http\Model\Fraisforfait) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(FraisforfaitTableMap::DATABASE_NAME);
            $criteria->add(FraisforfaitTableMap::COL_IDFRAISFORFAIT, (array) $values, Criteria::IN);
        }

        $query = FraisforfaitQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            FraisforfaitTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                FraisforfaitTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the fraisforfait table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(?ConnectionInterface $con = null): int
    {
        return FraisforfaitQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Fraisforfait or Criteria object.
     *
     * @param mixed $criteria Criteria or Fraisforfait object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed The new primary key.
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ?ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(FraisforfaitTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Fraisforfait object
        }


        // Set the correct dbName
        $query = FraisforfaitQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

}
