<?php

namespace App\Http\Model\Map;

use App\Http\Model\Lignefraisforfait;
use App\Http\Model\LignefraisforfaitQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'lignefraisforfait' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 */
class LignefraisforfaitTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    public const CLASS_NAME = '.Map.LignefraisforfaitTableMap';

    /**
     * The default database name for this class
     */
    public const DATABASE_NAME = 'ws-gsb-connection';

    /**
     * The table name for this class
     */
    public const TABLE_NAME = 'lignefraisforfait';

    /**
     * The PHP name of this class (PascalCase)
     */
    public const TABLE_PHP_NAME = 'Lignefraisforfait';

    /**
     * The related Propel class for this table
     */
    public const OM_CLASS = '\\App\\Http\\Model\\Lignefraisforfait';

    /**
     * A class that can be returned by this tableMap
     */
    public const CLASS_DEFAULT = 'Lignefraisforfait';

    /**
     * The total number of columns
     */
    public const NUM_COLUMNS = 3;

    /**
     * The number of lazy-loaded columns
     */
    public const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    public const NUM_HYDRATE_COLUMNS = 3;

    /**
     * the column name for the idFraisForfait field
     */
    public const COL_IDFRAISFORFAIT = 'lignefraisforfait.idFraisForfait';

    /**
     * the column name for the idFicheFrais field
     */
    public const COL_IDFICHEFRAIS = 'lignefraisforfait.idFicheFrais';

    /**
     * the column name for the quantite field
     */
    public const COL_QUANTITE = 'lignefraisforfait.quantite';

    /**
     * The default string format for model objects of the related table
     */
    public const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     *
     * @var array<string, mixed>
     */
    protected static $fieldNames = [
        self::TYPE_PHPNAME       => ['Idfraisforfait', 'Idfichefrais', 'Quantite', ],
        self::TYPE_CAMELNAME     => ['idfraisforfait', 'idfichefrais', 'quantite', ],
        self::TYPE_COLNAME       => [LignefraisforfaitTableMap::COL_IDFRAISFORFAIT, LignefraisforfaitTableMap::COL_IDFICHEFRAIS, LignefraisforfaitTableMap::COL_QUANTITE, ],
        self::TYPE_FIELDNAME     => ['idFraisForfait', 'idFicheFrais', 'quantite', ],
        self::TYPE_NUM           => [0, 1, 2, ]
    ];

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     *
     * @var array<string, mixed>
     */
    protected static $fieldKeys = [
        self::TYPE_PHPNAME       => ['Idfraisforfait' => 0, 'Idfichefrais' => 1, 'Quantite' => 2, ],
        self::TYPE_CAMELNAME     => ['idfraisforfait' => 0, 'idfichefrais' => 1, 'quantite' => 2, ],
        self::TYPE_COLNAME       => [LignefraisforfaitTableMap::COL_IDFRAISFORFAIT => 0, LignefraisforfaitTableMap::COL_IDFICHEFRAIS => 1, LignefraisforfaitTableMap::COL_QUANTITE => 2, ],
        self::TYPE_FIELDNAME     => ['idFraisForfait' => 0, 'idFicheFrais' => 1, 'quantite' => 2, ],
        self::TYPE_NUM           => [0, 1, 2, ]
    ];

    /**
     * Holds a list of column names and their normalized version.
     *
     * @var array<string>
     */
    protected $normalizedColumnNameMap = [
        'Idfraisforfait' => 'IDFRAISFORFAIT',
        'Lignefraisforfait.Idfraisforfait' => 'IDFRAISFORFAIT',
        'idfraisforfait' => 'IDFRAISFORFAIT',
        'lignefraisforfait.idfraisforfait' => 'IDFRAISFORFAIT',
        'LignefraisforfaitTableMap::COL_IDFRAISFORFAIT' => 'IDFRAISFORFAIT',
        'COL_IDFRAISFORFAIT' => 'IDFRAISFORFAIT',
        'idFraisForfait' => 'IDFRAISFORFAIT',
        'lignefraisforfait.idFraisForfait' => 'IDFRAISFORFAIT',
        'Idfichefrais' => 'IDFICHEFRAIS',
        'Lignefraisforfait.Idfichefrais' => 'IDFICHEFRAIS',
        'idfichefrais' => 'IDFICHEFRAIS',
        'lignefraisforfait.idfichefrais' => 'IDFICHEFRAIS',
        'LignefraisforfaitTableMap::COL_IDFICHEFRAIS' => 'IDFICHEFRAIS',
        'COL_IDFICHEFRAIS' => 'IDFICHEFRAIS',
        'idFicheFrais' => 'IDFICHEFRAIS',
        'lignefraisforfait.idFicheFrais' => 'IDFICHEFRAIS',
        'Quantite' => 'QUANTITE',
        'Lignefraisforfait.Quantite' => 'QUANTITE',
        'quantite' => 'QUANTITE',
        'lignefraisforfait.quantite' => 'QUANTITE',
        'LignefraisforfaitTableMap::COL_QUANTITE' => 'QUANTITE',
        'COL_QUANTITE' => 'QUANTITE',
    ];

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function initialize(): void
    {
        // attributes
        $this->setName('lignefraisforfait');
        $this->setPhpName('Lignefraisforfait');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\App\\Http\\Model\\Lignefraisforfait');
        $this->setPackage('');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('idFraisForfait', 'Idfraisforfait', 'VARCHAR', true, 3, null);
        $this->addPrimaryKey('idFicheFrais', 'Idfichefrais', 'INTEGER', true, null, null);
        $this->addColumn('quantite', 'Quantite', 'SMALLINT', true, null, null);
    }

    /**
     * Build the RelationMap objects for this table relationships
     *
     * @return void
     */
    public function buildRelations(): void
    {
    }

    /**
     * Adds an object to the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database. In some cases you may need to explicitly add objects
     * to the cache in order to ensure that the same objects are always returned by find*()
     * and findPk*() calls.
     *
     * @param \App\Http\Model\Lignefraisforfait $obj A \App\Http\Model\Lignefraisforfait object.
     * @param string|null $key Key (optional) to use for instance map (for performance boost if key was already calculated externally).
     *
     * @return void
     */
    public static function addInstanceToPool(Lignefraisforfait $obj, ?string $key = null): void
    {
        if (Propel::isInstancePoolingEnabled()) {
            if (null === $key) {
                $key = serialize([(null === $obj->getIdfraisforfait() || is_scalar($obj->getIdfraisforfait()) || is_callable([$obj->getIdfraisforfait(), '__toString']) ? (string) $obj->getIdfraisforfait() : $obj->getIdfraisforfait()), (null === $obj->getIdfichefrais() || is_scalar($obj->getIdfichefrais()) || is_callable([$obj->getIdfichefrais(), '__toString']) ? (string) $obj->getIdfichefrais() : $obj->getIdfichefrais())]);
            } // if key === null
            self::$instances[$key] = $obj;
        }
    }

    /**
     * Removes an object from the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doDelete
     * methods in your stub classes -- you may need to explicitly remove objects
     * from the cache in order to prevent returning objects that no longer exist.
     *
     * @param mixed $value A \App\Http\Model\Lignefraisforfait object or a primary key value.
     *
     * @return void
     */
    public static function removeInstanceFromPool($value): void
    {
        if (Propel::isInstancePoolingEnabled() && null !== $value) {
            if (is_object($value) && $value instanceof \App\Http\Model\Lignefraisforfait) {
                $key = serialize([(null === $value->getIdfraisforfait() || is_scalar($value->getIdfraisforfait()) || is_callable([$value->getIdfraisforfait(), '__toString']) ? (string) $value->getIdfraisforfait() : $value->getIdfraisforfait()), (null === $value->getIdfichefrais() || is_scalar($value->getIdfichefrais()) || is_callable([$value->getIdfichefrais(), '__toString']) ? (string) $value->getIdfichefrais() : $value->getIdfichefrais())]);

            } elseif (is_array($value) && count($value) === 2) {
                // assume we've been passed a primary key";
                $key = serialize([(null === $value[0] || is_scalar($value[0]) || is_callable([$value[0], '__toString']) ? (string) $value[0] : $value[0]), (null === $value[1] || is_scalar($value[1]) || is_callable([$value[1], '__toString']) ? (string) $value[1] : $value[1])]);
            } elseif ($value instanceof Criteria) {
                self::$instances = [];

                return;
            } else {
                $e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or \App\Http\Model\Lignefraisforfait object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value, true)));
                throw $e;
            }

            unset(self::$instances[$key]);
        }
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array $row Resultset row.
     * @param int $offset The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string|null The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow(array $row, int $offset = 0, string $indexType = TableMap::TYPE_NUM): ?string
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idfraisforfait', TableMap::TYPE_PHPNAME, $indexType)] === null && $row[TableMap::TYPE_NUM == $indexType ? 1 + $offset : static::translateFieldName('Idfichefrais', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return serialize([(null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idfraisforfait', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idfraisforfait', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idfraisforfait', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idfraisforfait', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Idfraisforfait', TableMap::TYPE_PHPNAME, $indexType)]), (null === $row[TableMap::TYPE_NUM == $indexType ? 1 + $offset : static::translateFieldName('Idfichefrais', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 1 + $offset : static::translateFieldName('Idfichefrais', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 1 + $offset : static::translateFieldName('Idfichefrais', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 1 + $offset : static::translateFieldName('Idfichefrais', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 1 + $offset : static::translateFieldName('Idfichefrais', TableMap::TYPE_PHPNAME, $indexType)])]);
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array $row Resultset row.
     * @param int $offset The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow(array $row, int $offset = 0, string $indexType = TableMap::TYPE_NUM)
    {
            $pks = [];

        $pks[] = (string) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Idfraisforfait', TableMap::TYPE_PHPNAME, $indexType)
        ];
        $pks[] = (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 1 + $offset
                : self::translateFieldName('Idfichefrais', TableMap::TYPE_PHPNAME, $indexType)
        ];

        return $pks;
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param bool $withPrefix Whether to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass(bool $withPrefix = true): string
    {
        return $withPrefix ? LignefraisforfaitTableMap::CLASS_DEFAULT : LignefraisforfaitTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array $row Row returned by DataFetcher->fetch().
     * @param int $offset The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array (Lignefraisforfait object, last column rank)
     */
    public static function populateObject(array $row, int $offset = 0, string $indexType = TableMap::TYPE_NUM): array
    {
        $key = LignefraisforfaitTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = LignefraisforfaitTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + LignefraisforfaitTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = LignefraisforfaitTableMap::OM_CLASS;
            /** @var Lignefraisforfait $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            LignefraisforfaitTableMap::addInstanceToPool($obj, $key);
        }

        return [$obj, $col];
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array<object>
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher): array
    {
        $results = [];

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = LignefraisforfaitTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = LignefraisforfaitTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Lignefraisforfait $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                LignefraisforfaitTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria Object containing the columns to add.
     * @param string|null $alias Optional table alias
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return void
     */
    public static function addSelectColumns(Criteria $criteria, ?string $alias = null): void
    {
        if (null === $alias) {
            $criteria->addSelectColumn(LignefraisforfaitTableMap::COL_IDFRAISFORFAIT);
            $criteria->addSelectColumn(LignefraisforfaitTableMap::COL_IDFICHEFRAIS);
            $criteria->addSelectColumn(LignefraisforfaitTableMap::COL_QUANTITE);
        } else {
            $criteria->addSelectColumn($alias . '.idFraisForfait');
            $criteria->addSelectColumn($alias . '.idFicheFrais');
            $criteria->addSelectColumn($alias . '.quantite');
        }
    }

    /**
     * Remove all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be removed as they are only loaded on demand.
     *
     * @param Criteria $criteria Object containing the columns to remove.
     * @param string|null $alias Optional table alias
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return void
     */
    public static function removeSelectColumns(Criteria $criteria, ?string $alias = null): void
    {
        if (null === $alias) {
            $criteria->removeSelectColumn(LignefraisforfaitTableMap::COL_IDFRAISFORFAIT);
            $criteria->removeSelectColumn(LignefraisforfaitTableMap::COL_IDFICHEFRAIS);
            $criteria->removeSelectColumn(LignefraisforfaitTableMap::COL_QUANTITE);
        } else {
            $criteria->removeSelectColumn($alias . '.idFraisForfait');
            $criteria->removeSelectColumn($alias . '.idFicheFrais');
            $criteria->removeSelectColumn($alias . '.quantite');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap(): TableMap
    {
        return Propel::getServiceContainer()->getDatabaseMap(LignefraisforfaitTableMap::DATABASE_NAME)->getTable(LignefraisforfaitTableMap::TABLE_NAME);
    }

    /**
     * Performs a DELETE on the database, given a Lignefraisforfait or Criteria object OR a primary key value.
     *
     * @param mixed $values Criteria or Lignefraisforfait object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ?ConnectionInterface $con = null): int
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(LignefraisforfaitTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \App\Http\Model\Lignefraisforfait) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(LignefraisforfaitTableMap::DATABASE_NAME);
            // primary key is composite; we therefore, expect
            // the primary key passed to be an array of pkey values
            if (count($values) == count($values, COUNT_RECURSIVE)) {
                // array is not multi-dimensional
                $values = [$values];
            }
            foreach ($values as $value) {
                $criterion = $criteria->getNewCriterion(LignefraisforfaitTableMap::COL_IDFRAISFORFAIT, $value[0]);
                $criterion->addAnd($criteria->getNewCriterion(LignefraisforfaitTableMap::COL_IDFICHEFRAIS, $value[1]));
                $criteria->addOr($criterion);
            }
        }

        $query = LignefraisforfaitQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            LignefraisforfaitTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                LignefraisforfaitTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the lignefraisforfait table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(?ConnectionInterface $con = null): int
    {
        return LignefraisforfaitQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Lignefraisforfait or Criteria object.
     *
     * @param mixed $criteria Criteria or Lignefraisforfait object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed The new primary key.
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ?ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(LignefraisforfaitTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Lignefraisforfait object
        }


        // Set the correct dbName
        $query = LignefraisforfaitQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

}
