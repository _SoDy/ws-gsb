<?php

namespace App\Http\Model;

use App\Http\Model\Base\EtatQuery as BaseEtatQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'etat' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
class EtatQuery extends BaseEtatQuery
{

}
