<?php

namespace App\Http\Model;

use App\Http\Model\Base\Lignefraisforfait as BaseLignefraisforfait;

/**
 * Skeleton subclass for representing a row from the 'lignefraisforfait' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
class Lignefraisforfait extends BaseLignefraisforfait
{

}
