<?php

namespace App\Http\Model;

use App\Http\Model\Base\Etat as BaseEtat;

/**
 * Skeleton subclass for representing a row from the 'etat' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
class Etat extends BaseEtat
{

}
