<?php

namespace App\Http\Model\Base;

use \Exception;
use \PDO;
use App\Http\Model\Utilisateur as ChildUtilisateur;
use App\Http\Model\UtilisateurQuery as ChildUtilisateurQuery;
use App\Http\Model\Map\UtilisateurTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the `utilisateur` table.
 *
 * @method     ChildUtilisateurQuery orderByIduser($order = Criteria::ASC) Order by the idUser column
 * @method     ChildUtilisateurQuery orderByNom($order = Criteria::ASC) Order by the nom column
 * @method     ChildUtilisateurQuery orderByPrenom($order = Criteria::ASC) Order by the prenom column
 * @method     ChildUtilisateurQuery orderByLogin($order = Criteria::ASC) Order by the login column
 * @method     ChildUtilisateurQuery orderByMdp($order = Criteria::ASC) Order by the mdp column
 * @method     ChildUtilisateurQuery orderByAdresse($order = Criteria::ASC) Order by the adresse column
 * @method     ChildUtilisateurQuery orderByVille($order = Criteria::ASC) Order by the ville column
 * @method     ChildUtilisateurQuery orderByCp($order = Criteria::ASC) Order by the cp column
 * @method     ChildUtilisateurQuery orderByDateembauche($order = Criteria::ASC) Order by the dateEmbauche column
 * @method     ChildUtilisateurQuery orderByProfil($order = Criteria::ASC) Order by the profil column
 *
 * @method     ChildUtilisateurQuery groupByIduser() Group by the idUser column
 * @method     ChildUtilisateurQuery groupByNom() Group by the nom column
 * @method     ChildUtilisateurQuery groupByPrenom() Group by the prenom column
 * @method     ChildUtilisateurQuery groupByLogin() Group by the login column
 * @method     ChildUtilisateurQuery groupByMdp() Group by the mdp column
 * @method     ChildUtilisateurQuery groupByAdresse() Group by the adresse column
 * @method     ChildUtilisateurQuery groupByVille() Group by the ville column
 * @method     ChildUtilisateurQuery groupByCp() Group by the cp column
 * @method     ChildUtilisateurQuery groupByDateembauche() Group by the dateEmbauche column
 * @method     ChildUtilisateurQuery groupByProfil() Group by the profil column
 *
 * @method     ChildUtilisateurQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildUtilisateurQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildUtilisateurQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildUtilisateurQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildUtilisateurQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildUtilisateurQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildUtilisateurQuery leftJoinFichefrais($relationAlias = null) Adds a LEFT JOIN clause to the query using the Fichefrais relation
 * @method     ChildUtilisateurQuery rightJoinFichefrais($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Fichefrais relation
 * @method     ChildUtilisateurQuery innerJoinFichefrais($relationAlias = null) Adds a INNER JOIN clause to the query using the Fichefrais relation
 *
 * @method     ChildUtilisateurQuery joinWithFichefrais($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Fichefrais relation
 *
 * @method     ChildUtilisateurQuery leftJoinWithFichefrais() Adds a LEFT JOIN clause and with to the query using the Fichefrais relation
 * @method     ChildUtilisateurQuery rightJoinWithFichefrais() Adds a RIGHT JOIN clause and with to the query using the Fichefrais relation
 * @method     ChildUtilisateurQuery innerJoinWithFichefrais() Adds a INNER JOIN clause and with to the query using the Fichefrais relation
 *
 * @method     \App\Http\Model\FichefraisQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildUtilisateur|null findOne(?ConnectionInterface $con = null) Return the first ChildUtilisateur matching the query
 * @method     ChildUtilisateur findOneOrCreate(?ConnectionInterface $con = null) Return the first ChildUtilisateur matching the query, or a new ChildUtilisateur object populated from the query conditions when no match is found
 *
 * @method     ChildUtilisateur|null findOneByIduser(string $idUser) Return the first ChildUtilisateur filtered by the idUser column
 * @method     ChildUtilisateur|null findOneByNom(string $nom) Return the first ChildUtilisateur filtered by the nom column
 * @method     ChildUtilisateur|null findOneByPrenom(string $prenom) Return the first ChildUtilisateur filtered by the prenom column
 * @method     ChildUtilisateur|null findOneByLogin(string $login) Return the first ChildUtilisateur filtered by the login column
 * @method     ChildUtilisateur|null findOneByMdp(string $mdp) Return the first ChildUtilisateur filtered by the mdp column
 * @method     ChildUtilisateur|null findOneByAdresse(string $adresse) Return the first ChildUtilisateur filtered by the adresse column
 * @method     ChildUtilisateur|null findOneByVille(string $ville) Return the first ChildUtilisateur filtered by the ville column
 * @method     ChildUtilisateur|null findOneByCp(string $cp) Return the first ChildUtilisateur filtered by the cp column
 * @method     ChildUtilisateur|null findOneByDateembauche(string $dateEmbauche) Return the first ChildUtilisateur filtered by the dateEmbauche column
 * @method     ChildUtilisateur|null findOneByProfil(string $profil) Return the first ChildUtilisateur filtered by the profil column
 *
 * @method     ChildUtilisateur requirePk($key, ?ConnectionInterface $con = null) Return the ChildUtilisateur by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUtilisateur requireOne(?ConnectionInterface $con = null) Return the first ChildUtilisateur matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildUtilisateur requireOneByIduser(string $idUser) Return the first ChildUtilisateur filtered by the idUser column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUtilisateur requireOneByNom(string $nom) Return the first ChildUtilisateur filtered by the nom column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUtilisateur requireOneByPrenom(string $prenom) Return the first ChildUtilisateur filtered by the prenom column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUtilisateur requireOneByLogin(string $login) Return the first ChildUtilisateur filtered by the login column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUtilisateur requireOneByMdp(string $mdp) Return the first ChildUtilisateur filtered by the mdp column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUtilisateur requireOneByAdresse(string $adresse) Return the first ChildUtilisateur filtered by the adresse column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUtilisateur requireOneByVille(string $ville) Return the first ChildUtilisateur filtered by the ville column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUtilisateur requireOneByCp(string $cp) Return the first ChildUtilisateur filtered by the cp column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUtilisateur requireOneByDateembauche(string $dateEmbauche) Return the first ChildUtilisateur filtered by the dateEmbauche column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUtilisateur requireOneByProfil(string $profil) Return the first ChildUtilisateur filtered by the profil column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildUtilisateur[]|Collection find(?ConnectionInterface $con = null) Return ChildUtilisateur objects based on current ModelCriteria
 * @psalm-method Collection&\Traversable<ChildUtilisateur> find(?ConnectionInterface $con = null) Return ChildUtilisateur objects based on current ModelCriteria
 *
 * @method     ChildUtilisateur[]|Collection findByIduser(string|array<string> $idUser) Return ChildUtilisateur objects filtered by the idUser column
 * @psalm-method Collection&\Traversable<ChildUtilisateur> findByIduser(string|array<string> $idUser) Return ChildUtilisateur objects filtered by the idUser column
 * @method     ChildUtilisateur[]|Collection findByNom(string|array<string> $nom) Return ChildUtilisateur objects filtered by the nom column
 * @psalm-method Collection&\Traversable<ChildUtilisateur> findByNom(string|array<string> $nom) Return ChildUtilisateur objects filtered by the nom column
 * @method     ChildUtilisateur[]|Collection findByPrenom(string|array<string> $prenom) Return ChildUtilisateur objects filtered by the prenom column
 * @psalm-method Collection&\Traversable<ChildUtilisateur> findByPrenom(string|array<string> $prenom) Return ChildUtilisateur objects filtered by the prenom column
 * @method     ChildUtilisateur[]|Collection findByLogin(string|array<string> $login) Return ChildUtilisateur objects filtered by the login column
 * @psalm-method Collection&\Traversable<ChildUtilisateur> findByLogin(string|array<string> $login) Return ChildUtilisateur objects filtered by the login column
 * @method     ChildUtilisateur[]|Collection findByMdp(string|array<string> $mdp) Return ChildUtilisateur objects filtered by the mdp column
 * @psalm-method Collection&\Traversable<ChildUtilisateur> findByMdp(string|array<string> $mdp) Return ChildUtilisateur objects filtered by the mdp column
 * @method     ChildUtilisateur[]|Collection findByAdresse(string|array<string> $adresse) Return ChildUtilisateur objects filtered by the adresse column
 * @psalm-method Collection&\Traversable<ChildUtilisateur> findByAdresse(string|array<string> $adresse) Return ChildUtilisateur objects filtered by the adresse column
 * @method     ChildUtilisateur[]|Collection findByVille(string|array<string> $ville) Return ChildUtilisateur objects filtered by the ville column
 * @psalm-method Collection&\Traversable<ChildUtilisateur> findByVille(string|array<string> $ville) Return ChildUtilisateur objects filtered by the ville column
 * @method     ChildUtilisateur[]|Collection findByCp(string|array<string> $cp) Return ChildUtilisateur objects filtered by the cp column
 * @psalm-method Collection&\Traversable<ChildUtilisateur> findByCp(string|array<string> $cp) Return ChildUtilisateur objects filtered by the cp column
 * @method     ChildUtilisateur[]|Collection findByDateembauche(string|array<string> $dateEmbauche) Return ChildUtilisateur objects filtered by the dateEmbauche column
 * @psalm-method Collection&\Traversable<ChildUtilisateur> findByDateembauche(string|array<string> $dateEmbauche) Return ChildUtilisateur objects filtered by the dateEmbauche column
 * @method     ChildUtilisateur[]|Collection findByProfil(string|array<string> $profil) Return ChildUtilisateur objects filtered by the profil column
 * @psalm-method Collection&\Traversable<ChildUtilisateur> findByProfil(string|array<string> $profil) Return ChildUtilisateur objects filtered by the profil column
 *
 * @method     ChildUtilisateur[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ?ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 * @psalm-method \Propel\Runtime\Util\PropelModelPager&\Traversable<ChildUtilisateur> paginate($page = 1, $maxPerPage = 10, ?ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 */
abstract class UtilisateurQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \App\Http\Model\Base\UtilisateurQuery object.
     *
     * @param string $dbName The database name
     * @param string $modelName The phpName of a model, e.g. 'Book'
     * @param string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'ws-gsb-connection', $modelName = '\\App\\Http\\Model\\Utilisateur', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildUtilisateurQuery object.
     *
     * @param string $modelAlias The alias of a model in the query
     * @param Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildUtilisateurQuery
     */
    public static function create(?string $modelAlias = null, ?Criteria $criteria = null): Criteria
    {
        if ($criteria instanceof ChildUtilisateurQuery) {
            return $criteria;
        }
        $query = new ChildUtilisateurQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildUtilisateur|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ?ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(UtilisateurTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = UtilisateurTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildUtilisateur A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT idUser, nom, prenom, login, mdp, adresse, ville, cp, dateEmbauche, profil FROM utilisateur WHERE idUser = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildUtilisateur $obj */
            $obj = new ChildUtilisateur();
            $obj->hydrate($row);
            UtilisateurTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con A connection object
     *
     * @return ChildUtilisateur|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param array $keys Primary keys to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return Collection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ?ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param mixed $key Primary key to use for the query
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        $this->addUsingAlias(UtilisateurTableMap::COL_IDUSER, $key, Criteria::EQUAL);

        return $this;
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param array|int $keys The list of primary key to use for the query
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        $this->addUsingAlias(UtilisateurTableMap::COL_IDUSER, $keys, Criteria::IN);

        return $this;
    }

    /**
     * Filter the query on the idUser column
     *
     * Example usage:
     * <code>
     * $query->filterByIduser('fooValue');   // WHERE idUser = 'fooValue'
     * $query->filterByIduser('%fooValue%', Criteria::LIKE); // WHERE idUser LIKE '%fooValue%'
     * $query->filterByIduser(['foo', 'bar']); // WHERE idUser IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $iduser The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByIduser($iduser = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($iduser)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(UtilisateurTableMap::COL_IDUSER, $iduser, $comparison);

        return $this;
    }

    /**
     * Filter the query on the nom column
     *
     * Example usage:
     * <code>
     * $query->filterByNom('fooValue');   // WHERE nom = 'fooValue'
     * $query->filterByNom('%fooValue%', Criteria::LIKE); // WHERE nom LIKE '%fooValue%'
     * $query->filterByNom(['foo', 'bar']); // WHERE nom IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $nom The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByNom($nom = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nom)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(UtilisateurTableMap::COL_NOM, $nom, $comparison);

        return $this;
    }

    /**
     * Filter the query on the prenom column
     *
     * Example usage:
     * <code>
     * $query->filterByPrenom('fooValue');   // WHERE prenom = 'fooValue'
     * $query->filterByPrenom('%fooValue%', Criteria::LIKE); // WHERE prenom LIKE '%fooValue%'
     * $query->filterByPrenom(['foo', 'bar']); // WHERE prenom IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $prenom The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByPrenom($prenom = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($prenom)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(UtilisateurTableMap::COL_PRENOM, $prenom, $comparison);

        return $this;
    }

    /**
     * Filter the query on the login column
     *
     * Example usage:
     * <code>
     * $query->filterByLogin('fooValue');   // WHERE login = 'fooValue'
     * $query->filterByLogin('%fooValue%', Criteria::LIKE); // WHERE login LIKE '%fooValue%'
     * $query->filterByLogin(['foo', 'bar']); // WHERE login IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $login The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByLogin($login = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($login)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(UtilisateurTableMap::COL_LOGIN, $login, $comparison);

        return $this;
    }

    /**
     * Filter the query on the mdp column
     *
     * Example usage:
     * <code>
     * $query->filterByMdp('fooValue');   // WHERE mdp = 'fooValue'
     * $query->filterByMdp('%fooValue%', Criteria::LIKE); // WHERE mdp LIKE '%fooValue%'
     * $query->filterByMdp(['foo', 'bar']); // WHERE mdp IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $mdp The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByMdp($mdp = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($mdp)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(UtilisateurTableMap::COL_MDP, $mdp, $comparison);

        return $this;
    }

    /**
     * Filter the query on the adresse column
     *
     * Example usage:
     * <code>
     * $query->filterByAdresse('fooValue');   // WHERE adresse = 'fooValue'
     * $query->filterByAdresse('%fooValue%', Criteria::LIKE); // WHERE adresse LIKE '%fooValue%'
     * $query->filterByAdresse(['foo', 'bar']); // WHERE adresse IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $adresse The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByAdresse($adresse = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($adresse)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(UtilisateurTableMap::COL_ADRESSE, $adresse, $comparison);

        return $this;
    }

    /**
     * Filter the query on the ville column
     *
     * Example usage:
     * <code>
     * $query->filterByVille('fooValue');   // WHERE ville = 'fooValue'
     * $query->filterByVille('%fooValue%', Criteria::LIKE); // WHERE ville LIKE '%fooValue%'
     * $query->filterByVille(['foo', 'bar']); // WHERE ville IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $ville The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByVille($ville = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ville)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(UtilisateurTableMap::COL_VILLE, $ville, $comparison);

        return $this;
    }

    /**
     * Filter the query on the cp column
     *
     * Example usage:
     * <code>
     * $query->filterByCp('fooValue');   // WHERE cp = 'fooValue'
     * $query->filterByCp('%fooValue%', Criteria::LIKE); // WHERE cp LIKE '%fooValue%'
     * $query->filterByCp(['foo', 'bar']); // WHERE cp IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $cp The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByCp($cp = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($cp)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(UtilisateurTableMap::COL_CP, $cp, $comparison);

        return $this;
    }

    /**
     * Filter the query on the dateEmbauche column
     *
     * Example usage:
     * <code>
     * $query->filterByDateembauche('2011-03-14'); // WHERE dateEmbauche = '2011-03-14'
     * $query->filterByDateembauche('now'); // WHERE dateEmbauche = '2011-03-14'
     * $query->filterByDateembauche(array('max' => 'yesterday')); // WHERE dateEmbauche > '2011-03-13'
     * </code>
     *
     * @param mixed $dateembauche The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByDateembauche($dateembauche = null, ?string $comparison = null)
    {
        if (is_array($dateembauche)) {
            $useMinMax = false;
            if (isset($dateembauche['min'])) {
                $this->addUsingAlias(UtilisateurTableMap::COL_DATEEMBAUCHE, $dateembauche['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateembauche['max'])) {
                $this->addUsingAlias(UtilisateurTableMap::COL_DATEEMBAUCHE, $dateembauche['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(UtilisateurTableMap::COL_DATEEMBAUCHE, $dateembauche, $comparison);

        return $this;
    }

    /**
     * Filter the query on the profil column
     *
     * Example usage:
     * <code>
     * $query->filterByProfil('fooValue');   // WHERE profil = 'fooValue'
     * $query->filterByProfil('%fooValue%', Criteria::LIKE); // WHERE profil LIKE '%fooValue%'
     * $query->filterByProfil(['foo', 'bar']); // WHERE profil IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $profil The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByProfil($profil = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($profil)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(UtilisateurTableMap::COL_PROFIL, $profil, $comparison);

        return $this;
    }

    /**
     * Filter the query by a related \App\Http\Model\Fichefrais object
     *
     * @param \App\Http\Model\Fichefrais|ObjectCollection $fichefrais the related object to use as filter
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByFichefrais($fichefrais, ?string $comparison = null)
    {
        if ($fichefrais instanceof \App\Http\Model\Fichefrais) {
            $this
                ->addUsingAlias(UtilisateurTableMap::COL_IDUSER, $fichefrais->getIdvisiteur(), $comparison);

            return $this;
        } elseif ($fichefrais instanceof ObjectCollection) {
            $this
                ->useFichefraisQuery()
                ->filterByPrimaryKeys($fichefrais->getPrimaryKeys())
                ->endUse();

            return $this;
        } else {
            throw new PropelException('filterByFichefrais() only accepts arguments of type \App\Http\Model\Fichefrais or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Fichefrais relation
     *
     * @param string|null $relationAlias Optional alias for the relation
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this The current query, for fluid interface
     */
    public function joinFichefrais(?string $relationAlias = null, ?string $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Fichefrais');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Fichefrais');
        }

        return $this;
    }

    /**
     * Use the Fichefrais relation Fichefrais object
     *
     * @see useQuery()
     *
     * @param string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \App\Http\Model\FichefraisQuery A secondary query class using the current class as primary query
     */
    public function useFichefraisQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinFichefrais($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Fichefrais', '\App\Http\Model\FichefraisQuery');
    }

    /**
     * Use the Fichefrais relation Fichefrais object
     *
     * @param callable(\App\Http\Model\FichefraisQuery):\App\Http\Model\FichefraisQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withFichefraisQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useFichefraisQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }

    /**
     * Use the relation to Fichefrais table for an EXISTS query.
     *
     * @see \Propel\Runtime\ActiveQuery\ModelCriteria::useExistsQuery()
     *
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     * @param string $typeOfExists Either ExistsQueryCriterion::TYPE_EXISTS or ExistsQueryCriterion::TYPE_NOT_EXISTS
     *
     * @return \App\Http\Model\FichefraisQuery The inner query object of the EXISTS statement
     */
    public function useFichefraisExistsQuery($modelAlias = null, $queryClass = null, $typeOfExists = 'EXISTS')
    {
        /** @var $q \App\Http\Model\FichefraisQuery */
        $q = $this->useExistsQuery('Fichefrais', $modelAlias, $queryClass, $typeOfExists);
        return $q;
    }

    /**
     * Use the relation to Fichefrais table for a NOT EXISTS query.
     *
     * @see useFichefraisExistsQuery()
     *
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     *
     * @return \App\Http\Model\FichefraisQuery The inner query object of the NOT EXISTS statement
     */
    public function useFichefraisNotExistsQuery($modelAlias = null, $queryClass = null)
    {
        /** @var $q \App\Http\Model\FichefraisQuery */
        $q = $this->useExistsQuery('Fichefrais', $modelAlias, $queryClass, 'NOT EXISTS');
        return $q;
    }

    /**
     * Use the relation to Fichefrais table for an IN query.
     *
     * @see \Propel\Runtime\ActiveQuery\ModelCriteria::useInQuery()
     *
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string|null $queryClass Allows to use a custom query class for the IN query, like ExtendedBookQuery::class
     * @param string $typeOfIn Criteria::IN or Criteria::NOT_IN
     *
     * @return \App\Http\Model\FichefraisQuery The inner query object of the IN statement
     */
    public function useInFichefraisQuery($modelAlias = null, $queryClass = null, $typeOfIn = 'IN')
    {
        /** @var $q \App\Http\Model\FichefraisQuery */
        $q = $this->useInQuery('Fichefrais', $modelAlias, $queryClass, $typeOfIn);
        return $q;
    }

    /**
     * Use the relation to Fichefrais table for a NOT IN query.
     *
     * @see useFichefraisInQuery()
     *
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string|null $queryClass Allows to use a custom query class for the NOT IN query, like ExtendedBookQuery::class
     *
     * @return \App\Http\Model\FichefraisQuery The inner query object of the NOT IN statement
     */
    public function useNotInFichefraisQuery($modelAlias = null, $queryClass = null)
    {
        /** @var $q \App\Http\Model\FichefraisQuery */
        $q = $this->useInQuery('Fichefrais', $modelAlias, $queryClass, 'NOT IN');
        return $q;
    }

    /**
     * Exclude object from result
     *
     * @param ChildUtilisateur $utilisateur Object to remove from the list of results
     *
     * @return $this The current query, for fluid interface
     */
    public function prune($utilisateur = null)
    {
        if ($utilisateur) {
            $this->addUsingAlias(UtilisateurTableMap::COL_IDUSER, $utilisateur->getIduser(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the utilisateur table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(?ConnectionInterface $con = null): int
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UtilisateurTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            UtilisateurTableMap::clearInstancePool();
            UtilisateurTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(?ConnectionInterface $con = null): int
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UtilisateurTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(UtilisateurTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            UtilisateurTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            UtilisateurTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

}
