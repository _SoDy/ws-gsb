<?php

namespace App\Http\Model\Base;

use \Exception;
use \PDO;
use App\Http\Model\Etat as ChildEtat;
use App\Http\Model\EtatQuery as ChildEtatQuery;
use App\Http\Model\Map\EtatTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the `etat` table.
 *
 * @method     ChildEtatQuery orderByIdetat($order = Criteria::ASC) Order by the idEtat column
 * @method     ChildEtatQuery orderByLibelleetat($order = Criteria::ASC) Order by the libelleEtat column
 *
 * @method     ChildEtatQuery groupByIdetat() Group by the idEtat column
 * @method     ChildEtatQuery groupByLibelleetat() Group by the libelleEtat column
 *
 * @method     ChildEtatQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildEtatQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildEtatQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildEtatQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildEtatQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildEtatQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildEtatQuery leftJoinFichefrais($relationAlias = null) Adds a LEFT JOIN clause to the query using the Fichefrais relation
 * @method     ChildEtatQuery rightJoinFichefrais($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Fichefrais relation
 * @method     ChildEtatQuery innerJoinFichefrais($relationAlias = null) Adds a INNER JOIN clause to the query using the Fichefrais relation
 *
 * @method     ChildEtatQuery joinWithFichefrais($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Fichefrais relation
 *
 * @method     ChildEtatQuery leftJoinWithFichefrais() Adds a LEFT JOIN clause and with to the query using the Fichefrais relation
 * @method     ChildEtatQuery rightJoinWithFichefrais() Adds a RIGHT JOIN clause and with to the query using the Fichefrais relation
 * @method     ChildEtatQuery innerJoinWithFichefrais() Adds a INNER JOIN clause and with to the query using the Fichefrais relation
 *
 * @method     \App\Http\Model\FichefraisQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildEtat|null findOne(?ConnectionInterface $con = null) Return the first ChildEtat matching the query
 * @method     ChildEtat findOneOrCreate(?ConnectionInterface $con = null) Return the first ChildEtat matching the query, or a new ChildEtat object populated from the query conditions when no match is found
 *
 * @method     ChildEtat|null findOneByIdetat(string $idEtat) Return the first ChildEtat filtered by the idEtat column
 * @method     ChildEtat|null findOneByLibelleetat(string $libelleEtat) Return the first ChildEtat filtered by the libelleEtat column
 *
 * @method     ChildEtat requirePk($key, ?ConnectionInterface $con = null) Return the ChildEtat by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEtat requireOne(?ConnectionInterface $con = null) Return the first ChildEtat matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildEtat requireOneByIdetat(string $idEtat) Return the first ChildEtat filtered by the idEtat column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEtat requireOneByLibelleetat(string $libelleEtat) Return the first ChildEtat filtered by the libelleEtat column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildEtat[]|Collection find(?ConnectionInterface $con = null) Return ChildEtat objects based on current ModelCriteria
 * @psalm-method Collection&\Traversable<ChildEtat> find(?ConnectionInterface $con = null) Return ChildEtat objects based on current ModelCriteria
 *
 * @method     ChildEtat[]|Collection findByIdetat(string|array<string> $idEtat) Return ChildEtat objects filtered by the idEtat column
 * @psalm-method Collection&\Traversable<ChildEtat> findByIdetat(string|array<string> $idEtat) Return ChildEtat objects filtered by the idEtat column
 * @method     ChildEtat[]|Collection findByLibelleetat(string|array<string> $libelleEtat) Return ChildEtat objects filtered by the libelleEtat column
 * @psalm-method Collection&\Traversable<ChildEtat> findByLibelleetat(string|array<string> $libelleEtat) Return ChildEtat objects filtered by the libelleEtat column
 *
 * @method     ChildEtat[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ?ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 * @psalm-method \Propel\Runtime\Util\PropelModelPager&\Traversable<ChildEtat> paginate($page = 1, $maxPerPage = 10, ?ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 */
abstract class EtatQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \App\Http\Model\Base\EtatQuery object.
     *
     * @param string $dbName The database name
     * @param string $modelName The phpName of a model, e.g. 'Book'
     * @param string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'ws-gsb-connection', $modelName = '\\App\\Http\\Model\\Etat', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildEtatQuery object.
     *
     * @param string $modelAlias The alias of a model in the query
     * @param Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildEtatQuery
     */
    public static function create(?string $modelAlias = null, ?Criteria $criteria = null): Criteria
    {
        if ($criteria instanceof ChildEtatQuery) {
            return $criteria;
        }
        $query = new ChildEtatQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildEtat|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ?ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(EtatTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = EtatTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildEtat A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT idEtat, libelleEtat FROM etat WHERE idEtat = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildEtat $obj */
            $obj = new ChildEtat();
            $obj->hydrate($row);
            EtatTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con A connection object
     *
     * @return ChildEtat|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param array $keys Primary keys to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return Collection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ?ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param mixed $key Primary key to use for the query
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        $this->addUsingAlias(EtatTableMap::COL_IDETAT, $key, Criteria::EQUAL);

        return $this;
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param array|int $keys The list of primary key to use for the query
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        $this->addUsingAlias(EtatTableMap::COL_IDETAT, $keys, Criteria::IN);

        return $this;
    }

    /**
     * Filter the query on the idEtat column
     *
     * Example usage:
     * <code>
     * $query->filterByIdetat('fooValue');   // WHERE idEtat = 'fooValue'
     * $query->filterByIdetat('%fooValue%', Criteria::LIKE); // WHERE idEtat LIKE '%fooValue%'
     * $query->filterByIdetat(['foo', 'bar']); // WHERE idEtat IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $idetat The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByIdetat($idetat = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($idetat)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(EtatTableMap::COL_IDETAT, $idetat, $comparison);

        return $this;
    }

    /**
     * Filter the query on the libelleEtat column
     *
     * Example usage:
     * <code>
     * $query->filterByLibelleetat('fooValue');   // WHERE libelleEtat = 'fooValue'
     * $query->filterByLibelleetat('%fooValue%', Criteria::LIKE); // WHERE libelleEtat LIKE '%fooValue%'
     * $query->filterByLibelleetat(['foo', 'bar']); // WHERE libelleEtat IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $libelleetat The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByLibelleetat($libelleetat = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($libelleetat)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(EtatTableMap::COL_LIBELLEETAT, $libelleetat, $comparison);

        return $this;
    }

    /**
     * Filter the query by a related \App\Http\Model\Fichefrais object
     *
     * @param \App\Http\Model\Fichefrais|ObjectCollection $fichefrais the related object to use as filter
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByFichefrais($fichefrais, ?string $comparison = null)
    {
        if ($fichefrais instanceof \App\Http\Model\Fichefrais) {
            $this
                ->addUsingAlias(EtatTableMap::COL_IDETAT, $fichefrais->getIdetat(), $comparison);

            return $this;
        } elseif ($fichefrais instanceof ObjectCollection) {
            $this
                ->useFichefraisQuery()
                ->filterByPrimaryKeys($fichefrais->getPrimaryKeys())
                ->endUse();

            return $this;
        } else {
            throw new PropelException('filterByFichefrais() only accepts arguments of type \App\Http\Model\Fichefrais or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Fichefrais relation
     *
     * @param string|null $relationAlias Optional alias for the relation
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this The current query, for fluid interface
     */
    public function joinFichefrais(?string $relationAlias = null, ?string $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Fichefrais');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Fichefrais');
        }

        return $this;
    }

    /**
     * Use the Fichefrais relation Fichefrais object
     *
     * @see useQuery()
     *
     * @param string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \App\Http\Model\FichefraisQuery A secondary query class using the current class as primary query
     */
    public function useFichefraisQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinFichefrais($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Fichefrais', '\App\Http\Model\FichefraisQuery');
    }

    /**
     * Use the Fichefrais relation Fichefrais object
     *
     * @param callable(\App\Http\Model\FichefraisQuery):\App\Http\Model\FichefraisQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withFichefraisQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useFichefraisQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }

    /**
     * Use the relation to Fichefrais table for an EXISTS query.
     *
     * @see \Propel\Runtime\ActiveQuery\ModelCriteria::useExistsQuery()
     *
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     * @param string $typeOfExists Either ExistsQueryCriterion::TYPE_EXISTS or ExistsQueryCriterion::TYPE_NOT_EXISTS
     *
     * @return \App\Http\Model\FichefraisQuery The inner query object of the EXISTS statement
     */
    public function useFichefraisExistsQuery($modelAlias = null, $queryClass = null, $typeOfExists = 'EXISTS')
    {
        /** @var $q \App\Http\Model\FichefraisQuery */
        $q = $this->useExistsQuery('Fichefrais', $modelAlias, $queryClass, $typeOfExists);
        return $q;
    }

    /**
     * Use the relation to Fichefrais table for a NOT EXISTS query.
     *
     * @see useFichefraisExistsQuery()
     *
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     *
     * @return \App\Http\Model\FichefraisQuery The inner query object of the NOT EXISTS statement
     */
    public function useFichefraisNotExistsQuery($modelAlias = null, $queryClass = null)
    {
        /** @var $q \App\Http\Model\FichefraisQuery */
        $q = $this->useExistsQuery('Fichefrais', $modelAlias, $queryClass, 'NOT EXISTS');
        return $q;
    }

    /**
     * Use the relation to Fichefrais table for an IN query.
     *
     * @see \Propel\Runtime\ActiveQuery\ModelCriteria::useInQuery()
     *
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string|null $queryClass Allows to use a custom query class for the IN query, like ExtendedBookQuery::class
     * @param string $typeOfIn Criteria::IN or Criteria::NOT_IN
     *
     * @return \App\Http\Model\FichefraisQuery The inner query object of the IN statement
     */
    public function useInFichefraisQuery($modelAlias = null, $queryClass = null, $typeOfIn = 'IN')
    {
        /** @var $q \App\Http\Model\FichefraisQuery */
        $q = $this->useInQuery('Fichefrais', $modelAlias, $queryClass, $typeOfIn);
        return $q;
    }

    /**
     * Use the relation to Fichefrais table for a NOT IN query.
     *
     * @see useFichefraisInQuery()
     *
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string|null $queryClass Allows to use a custom query class for the NOT IN query, like ExtendedBookQuery::class
     *
     * @return \App\Http\Model\FichefraisQuery The inner query object of the NOT IN statement
     */
    public function useNotInFichefraisQuery($modelAlias = null, $queryClass = null)
    {
        /** @var $q \App\Http\Model\FichefraisQuery */
        $q = $this->useInQuery('Fichefrais', $modelAlias, $queryClass, 'NOT IN');
        return $q;
    }

    /**
     * Exclude object from result
     *
     * @param ChildEtat $etat Object to remove from the list of results
     *
     * @return $this The current query, for fluid interface
     */
    public function prune($etat = null)
    {
        if ($etat) {
            $this->addUsingAlias(EtatTableMap::COL_IDETAT, $etat->getIdetat(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the etat table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(?ConnectionInterface $con = null): int
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(EtatTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            EtatTableMap::clearInstancePool();
            EtatTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(?ConnectionInterface $con = null): int
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(EtatTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(EtatTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            EtatTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            EtatTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

}
