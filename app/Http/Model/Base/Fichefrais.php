<?php

namespace App\Http\Model\Base;

use \Exception;
use \PDO;
use App\Http\Model\Etat as ChildEtat;
use App\Http\Model\EtatQuery as ChildEtatQuery;
use App\Http\Model\FichefraisQuery as ChildFichefraisQuery;
use App\Http\Model\Utilisateur as ChildUtilisateur;
use App\Http\Model\UtilisateurQuery as ChildUtilisateurQuery;
use App\Http\Model\Map\FichefraisTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;

/**
 * Base class that represents a row from the 'fichefrais' table.
 *
 *
 *
 * @package    propel.generator..Base
 */
abstract class Fichefrais implements ActiveRecordInterface
{
    /**
     * TableMap class name
     *
     * @var string
     */
    public const TABLE_MAP = '\\App\\Http\\Model\\Map\\FichefraisTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var bool
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var bool
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = [];

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = [];

    /**
     * The value for the idfichefrais field.
     *
     * @var        int
     */
    protected $idfichefrais;

    /**
     * The value for the moisannee field.
     *
     * @var        string
     */
    protected $moisannee;

    /**
     * The value for the nbjustificatifs field.
     *
     * @var        int|null
     */
    protected $nbjustificatifs;

    /**
     * The value for the montantvalide field.
     *
     * @var        string|null
     */
    protected $montantvalide;

    /**
     * The value for the datemodif field.
     *
     * @var        int|null
     */
    protected $datemodif;

    /**
     * The value for the idvisiteur field.
     *
     * @var        string
     */
    protected $idvisiteur;

    /**
     * The value for the idetat field.
     *
     * @var        string
     */
    protected $idetat;

    /**
     * @var        ChildEtat
     */
    protected $aEtat;

    /**
     * @var        ChildUtilisateur
     */
    protected $aUtilisateur;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var bool
     */
    protected $alreadyInSave = false;

    /**
     * Initializes internal state of App\Http\Model\Base\Fichefrais object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return bool True if the object has been modified.
     */
    public function isModified(): bool
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param string $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return bool True if $col has been modified.
     */
    public function isColumnModified(string $col): bool
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns(): array
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return bool True, if the object has never been persisted.
     */
    public function isNew(): bool
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param bool $b the state of the object.
     */
    public function setNew(bool $b): void
    {
        $this->new = $b;
    }

    /**
     * Whether this object has been deleted.
     * @return bool The deleted state of this object.
     */
    public function isDeleted(): bool
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param bool $b The deleted state of this object.
     * @return void
     */
    public function setDeleted(bool $b): void
    {
        $this->deleted = $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified(?string $col = null): void
    {
        if (null !== $col) {
            unset($this->modifiedColumns[$col]);
        } else {
            $this->modifiedColumns = [];
        }
    }

    /**
     * Compares this with another <code>Fichefrais</code> instance.  If
     * <code>obj</code> is an instance of <code>Fichefrais</code>, delegates to
     * <code>equals(Fichefrais)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param mixed $obj The object to compare to.
     * @return bool Whether equal to the object specified.
     */
    public function equals($obj): bool
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns(): array
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param string $name The virtual column name
     * @return bool
     */
    public function hasVirtualColumn(string $name): bool
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param string $name The virtual column name
     * @return mixed
     *
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function getVirtualColumn(string $name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of nonexistent virtual column `%s`.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name The virtual column name
     * @param mixed $value The value to give to the virtual column
     *
     * @return $this The current object, for fluid interface
     */
    public function setVirtualColumn(string $name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param string $msg
     * @param int $priority One of the Propel::LOG_* logging levels
     * @return void
     */
    protected function log(string $msg, int $priority = Propel::LOG_INFO): void
    {
        Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param \Propel\Runtime\Parser\AbstractParser|string $parser An AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param bool $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @param string $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME, TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM. Defaults to TableMap::TYPE_PHPNAME.
     * @return string The exported data
     */
    public function exportTo($parser, bool $includeLazyLoadColumns = true, string $keyType = TableMap::TYPE_PHPNAME): string
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray($keyType, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     *
     * @return array<string>
     */
    public function __sleep(): array
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [idfichefrais] column value.
     *
     * @return int
     */
    public function getIdfichefrais()
    {
        return $this->idfichefrais;
    }

    /**
     * Get the [moisannee] column value.
     *
     * @return string
     */
    public function getMoisannee()
    {
        return $this->moisannee;
    }

    /**
     * Get the [nbjustificatifs] column value.
     *
     * @return int|null
     */
    public function getNbjustificatifs()
    {
        return $this->nbjustificatifs;
    }

    /**
     * Get the [montantvalide] column value.
     *
     * @return string|null
     */
    public function getMontantvalide()
    {
        return $this->montantvalide;
    }

    /**
     * Get the [datemodif] column value.
     *
     * @return int|null
     */
    public function getDatemodif()
    {
        return $this->datemodif;
    }

    /**
     * Get the [idvisiteur] column value.
     *
     * @return string
     */
    public function getIdvisiteur()
    {
        return $this->idvisiteur;
    }

    /**
     * Get the [idetat] column value.
     *
     * @return string
     */
    public function getIdetat()
    {
        return $this->idetat;
    }

    /**
     * Set the value of [idfichefrais] column.
     *
     * @param int $v New value
     * @return $this The current object (for fluent API support)
     */
    public function setIdfichefrais($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->idfichefrais !== $v) {
            $this->idfichefrais = $v;
            $this->modifiedColumns[FichefraisTableMap::COL_IDFICHEFRAIS] = true;
        }

        return $this;
    }

    /**
     * Set the value of [moisannee] column.
     *
     * @param string $v New value
     * @return $this The current object (for fluent API support)
     */
    public function setMoisannee($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->moisannee !== $v) {
            $this->moisannee = $v;
            $this->modifiedColumns[FichefraisTableMap::COL_MOISANNEE] = true;
        }

        return $this;
    }

    /**
     * Set the value of [nbjustificatifs] column.
     *
     * @param int|null $v New value
     * @return $this The current object (for fluent API support)
     */
    public function setNbjustificatifs($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->nbjustificatifs !== $v) {
            $this->nbjustificatifs = $v;
            $this->modifiedColumns[FichefraisTableMap::COL_NBJUSTIFICATIFS] = true;
        }

        return $this;
    }

    /**
     * Set the value of [montantvalide] column.
     *
     * @param string|null $v New value
     * @return $this The current object (for fluent API support)
     */
    public function setMontantvalide($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->montantvalide !== $v) {
            $this->montantvalide = $v;
            $this->modifiedColumns[FichefraisTableMap::COL_MONTANTVALIDE] = true;
        }

        return $this;
    }

    /**
     * Set the value of [datemodif] column.
     *
     * @param int|null $v New value
     * @return $this The current object (for fluent API support)
     */
    public function setDatemodif($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->datemodif !== $v) {
            $this->datemodif = $v;
            $this->modifiedColumns[FichefraisTableMap::COL_DATEMODIF] = true;
        }

        return $this;
    }

    /**
     * Set the value of [idvisiteur] column.
     *
     * @param string $v New value
     * @return $this The current object (for fluent API support)
     */
    public function setIdvisiteur($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->idvisiteur !== $v) {
            $this->idvisiteur = $v;
            $this->modifiedColumns[FichefraisTableMap::COL_IDVISITEUR] = true;
        }

        if ($this->aUtilisateur !== null && $this->aUtilisateur->getIduser() !== $v) {
            $this->aUtilisateur = null;
        }

        return $this;
    }

    /**
     * Set the value of [idetat] column.
     *
     * @param string $v New value
     * @return $this The current object (for fluent API support)
     */
    public function setIdetat($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->idetat !== $v) {
            $this->idetat = $v;
            $this->modifiedColumns[FichefraisTableMap::COL_IDETAT] = true;
        }

        if ($this->aEtat !== null && $this->aEtat->getIdetat() !== $v) {
            $this->aEtat = null;
        }

        return $this;
    }

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return bool Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues(): bool
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    }

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by DataFetcher->fetch().
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param bool $rehydrate Whether this object is being re-hydrated from the database.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int next starting column
     * @throws \Propel\Runtime\Exception\PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate(array $row, int $startcol = 0, bool $rehydrate = false, string $indexType = TableMap::TYPE_NUM): int
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : FichefraisTableMap::translateFieldName('Idfichefrais', TableMap::TYPE_PHPNAME, $indexType)];
            $this->idfichefrais = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : FichefraisTableMap::translateFieldName('Moisannee', TableMap::TYPE_PHPNAME, $indexType)];
            $this->moisannee = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : FichefraisTableMap::translateFieldName('Nbjustificatifs', TableMap::TYPE_PHPNAME, $indexType)];
            $this->nbjustificatifs = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : FichefraisTableMap::translateFieldName('Montantvalide', TableMap::TYPE_PHPNAME, $indexType)];
            $this->montantvalide = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : FichefraisTableMap::translateFieldName('Datemodif', TableMap::TYPE_PHPNAME, $indexType)];
            $this->datemodif = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : FichefraisTableMap::translateFieldName('Idvisiteur', TableMap::TYPE_PHPNAME, $indexType)];
            $this->idvisiteur = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : FichefraisTableMap::translateFieldName('Idetat', TableMap::TYPE_PHPNAME, $indexType)];
            $this->idetat = (null !== $col) ? (string) $col : null;

            $this->resetModified();
            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 7; // 7 = FichefraisTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\App\\Http\\Model\\Fichefrais'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws \Propel\Runtime\Exception\PropelException
     * @return void
     */
    public function ensureConsistency(): void
    {
        if ($this->aUtilisateur !== null && $this->idvisiteur !== $this->aUtilisateur->getIduser()) {
            $this->aUtilisateur = null;
        }
        if ($this->aEtat !== null && $this->idetat !== $this->aEtat->getIdetat()) {
            $this->aEtat = null;
        }
    }

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param bool $deep (optional) Whether to also de-associated any related objects.
     * @param ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws \Propel\Runtime\Exception\PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload(bool $deep = false, ?ConnectionInterface $con = null): void
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(FichefraisTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildFichefraisQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aEtat = null;
            $this->aUtilisateur = null;
        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param ConnectionInterface $con
     * @return void
     * @throws \Propel\Runtime\Exception\PropelException
     * @see Fichefrais::setDeleted()
     * @see Fichefrais::isDeleted()
     */
    public function delete(?ConnectionInterface $con = null): void
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(FichefraisTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildFichefraisQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param ConnectionInterface $con
     * @return int The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws \Propel\Runtime\Exception\PropelException
     * @see doSave()
     */
    public function save(?ConnectionInterface $con = null): int
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(FichefraisTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                FichefraisTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param ConnectionInterface $con
     * @return int The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws \Propel\Runtime\Exception\PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con): int
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aEtat !== null) {
                if ($this->aEtat->isModified() || $this->aEtat->isNew()) {
                    $affectedRows += $this->aEtat->save($con);
                }
                $this->setEtat($this->aEtat);
            }

            if ($this->aUtilisateur !== null) {
                if ($this->aUtilisateur->isModified() || $this->aUtilisateur->isNew()) {
                    $affectedRows += $this->aUtilisateur->save($con);
                }
                $this->setUtilisateur($this->aUtilisateur);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    }

    /**
     * Insert the row in the database.
     *
     * @param ConnectionInterface $con
     *
     * @throws \Propel\Runtime\Exception\PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con): void
    {
        $modifiedColumns = [];
        $index = 0;

        $this->modifiedColumns[FichefraisTableMap::COL_IDFICHEFRAIS] = true;
        if (null !== $this->idfichefrais) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . FichefraisTableMap::COL_IDFICHEFRAIS . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(FichefraisTableMap::COL_IDFICHEFRAIS)) {
            $modifiedColumns[':p' . $index++]  = 'idFicheFrais';
        }
        if ($this->isColumnModified(FichefraisTableMap::COL_MOISANNEE)) {
            $modifiedColumns[':p' . $index++]  = 'moisAnnee';
        }
        if ($this->isColumnModified(FichefraisTableMap::COL_NBJUSTIFICATIFS)) {
            $modifiedColumns[':p' . $index++]  = 'nbJustificatifs';
        }
        if ($this->isColumnModified(FichefraisTableMap::COL_MONTANTVALIDE)) {
            $modifiedColumns[':p' . $index++]  = 'montantValide';
        }
        if ($this->isColumnModified(FichefraisTableMap::COL_DATEMODIF)) {
            $modifiedColumns[':p' . $index++]  = 'dateModif';
        }
        if ($this->isColumnModified(FichefraisTableMap::COL_IDVISITEUR)) {
            $modifiedColumns[':p' . $index++]  = 'idVisiteur';
        }
        if ($this->isColumnModified(FichefraisTableMap::COL_IDETAT)) {
            $modifiedColumns[':p' . $index++]  = 'idEtat';
        }

        $sql = sprintf(
            'INSERT INTO fichefrais (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'idFicheFrais':
                        $stmt->bindValue($identifier, $this->idfichefrais, PDO::PARAM_INT);

                        break;
                    case 'moisAnnee':
                        $stmt->bindValue($identifier, $this->moisannee, PDO::PARAM_STR);

                        break;
                    case 'nbJustificatifs':
                        $stmt->bindValue($identifier, $this->nbjustificatifs, PDO::PARAM_INT);

                        break;
                    case 'montantValide':
                        $stmt->bindValue($identifier, $this->montantvalide, PDO::PARAM_STR);

                        break;
                    case 'dateModif':
                        $stmt->bindValue($identifier, $this->datemodif, PDO::PARAM_INT);

                        break;
                    case 'idVisiteur':
                        $stmt->bindValue($identifier, $this->idvisiteur, PDO::PARAM_STR);

                        break;
                    case 'idEtat':
                        $stmt->bindValue($identifier, $this->idetat, PDO::PARAM_STR);

                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setIdfichefrais($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param ConnectionInterface $con
     *
     * @return int Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con): int
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName(string $name, string $type = TableMap::TYPE_PHPNAME)
    {
        $pos = FichefraisTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos Position in XML schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition(int $pos)
    {
        switch ($pos) {
            case 0:
                return $this->getIdfichefrais();

            case 1:
                return $this->getMoisannee();

            case 2:
                return $this->getNbjustificatifs();

            case 3:
                return $this->getMontantvalide();

            case 4:
                return $this->getDatemodif();

            case 5:
                return $this->getIdvisiteur();

            case 6:
                return $this->getIdetat();

            default:
                return null;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param string $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param bool $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param bool $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array An associative array containing the field names (as keys) and field values
     */
    public function toArray(string $keyType = TableMap::TYPE_PHPNAME, bool $includeLazyLoadColumns = true, array $alreadyDumpedObjects = [], bool $includeForeignObjects = false): array
    {
        if (isset($alreadyDumpedObjects['Fichefrais'][$this->hashCode()])) {
            return ['*RECURSION*'];
        }
        $alreadyDumpedObjects['Fichefrais'][$this->hashCode()] = true;
        $keys = FichefraisTableMap::getFieldNames($keyType);
        $result = [
            $keys[0] => $this->getIdfichefrais(),
            $keys[1] => $this->getMoisannee(),
            $keys[2] => $this->getNbjustificatifs(),
            $keys[3] => $this->getMontantvalide(),
            $keys[4] => $this->getDatemodif(),
            $keys[5] => $this->getIdvisiteur(),
            $keys[6] => $this->getIdetat(),
        ];
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aEtat) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'etat';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'etat';
                        break;
                    default:
                        $key = 'Etat';
                }

                $result[$key] = $this->aEtat->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aUtilisateur) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'utilisateur';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'utilisateur';
                        break;
                    default:
                        $key = 'Utilisateur';
                }

                $result[$key] = $this->aUtilisateur->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this
     */
    public function setByName(string $name, $value, string $type = TableMap::TYPE_PHPNAME)
    {
        $pos = FichefraisTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        $this->setByPosition($pos, $value);

        return $this;
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return $this
     */
    public function setByPosition(int $pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setIdfichefrais($value);
                break;
            case 1:
                $this->setMoisannee($value);
                break;
            case 2:
                $this->setNbjustificatifs($value);
                break;
            case 3:
                $this->setMontantvalide($value);
                break;
            case 4:
                $this->setDatemodif($value);
                break;
            case 5:
                $this->setIdvisiteur($value);
                break;
            case 6:
                $this->setIdetat($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param array $arr An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return $this
     */
    public function fromArray(array $arr, string $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = FichefraisTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setIdfichefrais($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setMoisannee($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setNbjustificatifs($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setMontantvalide($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setDatemodif($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setIdvisiteur($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setIdetat($arr[$keys[6]]);
        }

        return $this;
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this The current object, for fluid interface
     */
    public function importFrom($parser, string $data, string $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return \Propel\Runtime\ActiveQuery\Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria(): Criteria
    {
        $criteria = new Criteria(FichefraisTableMap::DATABASE_NAME);

        if ($this->isColumnModified(FichefraisTableMap::COL_IDFICHEFRAIS)) {
            $criteria->add(FichefraisTableMap::COL_IDFICHEFRAIS, $this->idfichefrais);
        }
        if ($this->isColumnModified(FichefraisTableMap::COL_MOISANNEE)) {
            $criteria->add(FichefraisTableMap::COL_MOISANNEE, $this->moisannee);
        }
        if ($this->isColumnModified(FichefraisTableMap::COL_NBJUSTIFICATIFS)) {
            $criteria->add(FichefraisTableMap::COL_NBJUSTIFICATIFS, $this->nbjustificatifs);
        }
        if ($this->isColumnModified(FichefraisTableMap::COL_MONTANTVALIDE)) {
            $criteria->add(FichefraisTableMap::COL_MONTANTVALIDE, $this->montantvalide);
        }
        if ($this->isColumnModified(FichefraisTableMap::COL_DATEMODIF)) {
            $criteria->add(FichefraisTableMap::COL_DATEMODIF, $this->datemodif);
        }
        if ($this->isColumnModified(FichefraisTableMap::COL_IDVISITEUR)) {
            $criteria->add(FichefraisTableMap::COL_IDVISITEUR, $this->idvisiteur);
        }
        if ($this->isColumnModified(FichefraisTableMap::COL_IDETAT)) {
            $criteria->add(FichefraisTableMap::COL_IDETAT, $this->idetat);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return \Propel\Runtime\ActiveQuery\Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria(): Criteria
    {
        $criteria = ChildFichefraisQuery::create();
        $criteria->add(FichefraisTableMap::COL_IDFICHEFRAIS, $this->idfichefrais);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int|string Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getIdfichefrais();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getIdfichefrais();
    }

    /**
     * Generic method to set the primary key (idfichefrais column).
     *
     * @param int|null $key Primary key.
     * @return void
     */
    public function setPrimaryKey(?int $key = null): void
    {
        $this->setIdfichefrais($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     *
     * @return bool
     */
    public function isPrimaryKeyNull(): bool
    {
        return null === $this->getIdfichefrais();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of \App\Http\Model\Fichefrais (or compatible) type.
     * @param bool $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param bool $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws \Propel\Runtime\Exception\PropelException
     * @return void
     */
    public function copyInto(object $copyObj, bool $deepCopy = false, bool $makeNew = true): void
    {
        $copyObj->setMoisannee($this->getMoisannee());
        $copyObj->setNbjustificatifs($this->getNbjustificatifs());
        $copyObj->setMontantvalide($this->getMontantvalide());
        $copyObj->setDatemodif($this->getDatemodif());
        $copyObj->setIdvisiteur($this->getIdvisiteur());
        $copyObj->setIdetat($this->getIdetat());
        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setIdfichefrais(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param bool $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \App\Http\Model\Fichefrais Clone of current object.
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function copy(bool $deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildEtat object.
     *
     * @param ChildEtat $v
     * @return $this The current object (for fluent API support)
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function setEtat(ChildEtat $v = null)
    {
        if ($v === null) {
            $this->setIdetat(NULL);
        } else {
            $this->setIdetat($v->getIdetat());
        }

        $this->aEtat = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildEtat object, it will not be re-added.
        if ($v !== null) {
            $v->addFichefrais($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildEtat object
     *
     * @param ConnectionInterface $con Optional Connection object.
     * @return ChildEtat The associated ChildEtat object.
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function getEtat(?ConnectionInterface $con = null)
    {
        if ($this->aEtat === null && (($this->idetat !== "" && $this->idetat !== null))) {
            $this->aEtat = ChildEtatQuery::create()->findPk($this->idetat, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aEtat->addFichefraiss($this);
             */
        }

        return $this->aEtat;
    }

    /**
     * Declares an association between this object and a ChildUtilisateur object.
     *
     * @param ChildUtilisateur $v
     * @return $this The current object (for fluent API support)
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function setUtilisateur(ChildUtilisateur $v = null)
    {
        if ($v === null) {
            $this->setIdvisiteur(NULL);
        } else {
            $this->setIdvisiteur($v->getIduser());
        }

        $this->aUtilisateur = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildUtilisateur object, it will not be re-added.
        if ($v !== null) {
            $v->addFichefrais($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildUtilisateur object
     *
     * @param ConnectionInterface $con Optional Connection object.
     * @return ChildUtilisateur The associated ChildUtilisateur object.
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function getUtilisateur(?ConnectionInterface $con = null)
    {
        if ($this->aUtilisateur === null && (($this->idvisiteur !== "" && $this->idvisiteur !== null))) {
            $this->aUtilisateur = ChildUtilisateurQuery::create()->findPk($this->idvisiteur, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aUtilisateur->addFichefraiss($this);
             */
        }

        return $this->aUtilisateur;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     *
     * @return $this
     */
    public function clear()
    {
        if (null !== $this->aEtat) {
            $this->aEtat->removeFichefrais($this);
        }
        if (null !== $this->aUtilisateur) {
            $this->aUtilisateur->removeFichefrais($this);
        }
        $this->idfichefrais = null;
        $this->moisannee = null;
        $this->nbjustificatifs = null;
        $this->montantvalide = null;
        $this->datemodif = null;
        $this->idvisiteur = null;
        $this->idetat = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);

        return $this;
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param bool $deep Whether to also clear the references on all referrer objects.
     * @return $this
     */
    public function clearAllReferences(bool $deep = false)
    {
        if ($deep) {
        } // if ($deep)

        $this->aEtat = null;
        $this->aUtilisateur = null;
        return $this;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(FichefraisTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param ConnectionInterface|null $con
     * @return bool
     */
    public function preSave(?ConnectionInterface $con = null): bool
    {
                return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface|null $con
     * @return void
     */
    public function postSave(?ConnectionInterface $con = null): void
    {
            }

    /**
     * Code to be run before inserting to database
     * @param ConnectionInterface|null $con
     * @return bool
     */
    public function preInsert(?ConnectionInterface $con = null): bool
    {
                return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface|null $con
     * @return void
     */
    public function postInsert(?ConnectionInterface $con = null): void
    {
            }

    /**
     * Code to be run before updating the object in database
     * @param ConnectionInterface|null $con
     * @return bool
     */
    public function preUpdate(?ConnectionInterface $con = null): bool
    {
                return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface|null $con
     * @return void
     */
    public function postUpdate(?ConnectionInterface $con = null): void
    {
            }

    /**
     * Code to be run before deleting the object in database
     * @param ConnectionInterface|null $con
     * @return bool
     */
    public function preDelete(?ConnectionInterface $con = null): bool
    {
                return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface|null $con
     * @return void
     */
    public function postDelete(?ConnectionInterface $con = null): void
    {
            }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);
            $inputData = $params[0];
            $keyType = $params[1] ?? TableMap::TYPE_PHPNAME;

            return $this->importFrom($format, $inputData, $keyType);
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = $params[0] ?? true;
            $keyType = $params[1] ?? TableMap::TYPE_PHPNAME;

            return $this->exportTo($format, $includeLazyLoadColumns, $keyType);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
