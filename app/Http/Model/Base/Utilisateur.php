<?php

namespace App\Http\Model\Base;

use \DateTime;
use \Exception;
use \PDO;
use App\Http\Model\Fichefrais as ChildFichefrais;
use App\Http\Model\FichefraisQuery as ChildFichefraisQuery;
use App\Http\Model\Utilisateur as ChildUtilisateur;
use App\Http\Model\UtilisateurQuery as ChildUtilisateurQuery;
use App\Http\Model\Map\FichefraisTableMap;
use App\Http\Model\Map\UtilisateurTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'utilisateur' table.
 *
 *
 *
 * @package    propel.generator..Base
 */
abstract class Utilisateur implements ActiveRecordInterface
{
    /**
     * TableMap class name
     *
     * @var string
     */
    public const TABLE_MAP = '\\App\\Http\\Model\\Map\\UtilisateurTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var bool
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var bool
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = [];

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = [];

    /**
     * The value for the iduser field.
     *
     * @var        string
     */
    protected $iduser;

    /**
     * The value for the nom field.
     *
     * @var        string
     */
    protected $nom;

    /**
     * The value for the prenom field.
     *
     * @var        string
     */
    protected $prenom;

    /**
     * The value for the login field.
     *
     * @var        string
     */
    protected $login;

    /**
     * The value for the mdp field.
     *
     * @var        string
     */
    protected $mdp;

    /**
     * The value for the adresse field.
     *
     * @var        string
     */
    protected $adresse;

    /**
     * The value for the ville field.
     *
     * @var        string
     */
    protected $ville;

    /**
     * The value for the cp field.
     *
     * @var        string
     */
    protected $cp;

    /**
     * The value for the dateembauche field.
     *
     * @var        DateTime
     */
    protected $dateembauche;

    /**
     * The value for the profil field.
     *
     * @var        string
     */
    protected $profil;

    /**
     * @var        ObjectCollection|ChildFichefrais[] Collection to store aggregation of ChildFichefrais objects.
     * @phpstan-var ObjectCollection&\Traversable<ChildFichefrais> Collection to store aggregation of ChildFichefrais objects.
     */
    protected $collFichefraiss;
    protected $collFichefraissPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var bool
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildFichefrais[]
     * @phpstan-var ObjectCollection&\Traversable<ChildFichefrais>
     */
    protected $fichefraissScheduledForDeletion = null;

    /**
     * Initializes internal state of App\Http\Model\Base\Utilisateur object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return bool True if the object has been modified.
     */
    public function isModified(): bool
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param string $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return bool True if $col has been modified.
     */
    public function isColumnModified(string $col): bool
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns(): array
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return bool True, if the object has never been persisted.
     */
    public function isNew(): bool
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param bool $b the state of the object.
     */
    public function setNew(bool $b): void
    {
        $this->new = $b;
    }

    /**
     * Whether this object has been deleted.
     * @return bool The deleted state of this object.
     */
    public function isDeleted(): bool
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param bool $b The deleted state of this object.
     * @return void
     */
    public function setDeleted(bool $b): void
    {
        $this->deleted = $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified(?string $col = null): void
    {
        if (null !== $col) {
            unset($this->modifiedColumns[$col]);
        } else {
            $this->modifiedColumns = [];
        }
    }

    /**
     * Compares this with another <code>Utilisateur</code> instance.  If
     * <code>obj</code> is an instance of <code>Utilisateur</code>, delegates to
     * <code>equals(Utilisateur)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param mixed $obj The object to compare to.
     * @return bool Whether equal to the object specified.
     */
    public function equals($obj): bool
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns(): array
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param string $name The virtual column name
     * @return bool
     */
    public function hasVirtualColumn(string $name): bool
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param string $name The virtual column name
     * @return mixed
     *
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function getVirtualColumn(string $name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of nonexistent virtual column `%s`.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name The virtual column name
     * @param mixed $value The value to give to the virtual column
     *
     * @return $this The current object, for fluid interface
     */
    public function setVirtualColumn(string $name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param string $msg
     * @param int $priority One of the Propel::LOG_* logging levels
     * @return void
     */
    protected function log(string $msg, int $priority = Propel::LOG_INFO): void
    {
        Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param \Propel\Runtime\Parser\AbstractParser|string $parser An AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param bool $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @param string $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME, TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM. Defaults to TableMap::TYPE_PHPNAME.
     * @return string The exported data
     */
    public function exportTo($parser, bool $includeLazyLoadColumns = true, string $keyType = TableMap::TYPE_PHPNAME): string
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray($keyType, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     *
     * @return array<string>
     */
    public function __sleep(): array
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [iduser] column value.
     *
     * @return string
     */
    public function getIduser()
    {
        return $this->iduser;
    }

    /**
     * Get the [nom] column value.
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Get the [prenom] column value.
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Get the [login] column value.
     *
     * @return string
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * Get the [mdp] column value.
     *
     * @return string
     */
    public function getMdp()
    {
        return $this->mdp;
    }

    /**
     * Get the [adresse] column value.
     *
     * @return string
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Get the [ville] column value.
     *
     * @return string
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Get the [cp] column value.
     *
     * @return string
     */
    public function getCp()
    {
        return $this->cp;
    }

    /**
     * Get the [optionally formatted] temporal [dateembauche] column value.
     *
     *
     * @param string|null $format The date/time format string (either date()-style or strftime()-style).
     *   If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), and 0 if column value is 0000-00-00.
     *
     * @throws \Propel\Runtime\Exception\PropelException - if unable to parse/validate the date/time value.
     *
     * @psalm-return ($format is null ? DateTime : string)
     */
    public function getDateembauche($format = null)
    {
        if ($format === null) {
            return $this->dateembauche;
        } else {
            return $this->dateembauche instanceof \DateTimeInterface ? $this->dateembauche->format($format) : null;
        }
    }

    /**
     * Get the [profil] column value.
     *
     * @return string
     */
    public function getProfil()
    {
        return $this->profil;
    }

    /**
     * Set the value of [iduser] column.
     *
     * @param string $v New value
     * @return $this The current object (for fluent API support)
     */
    public function setIduser($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->iduser !== $v) {
            $this->iduser = $v;
            $this->modifiedColumns[UtilisateurTableMap::COL_IDUSER] = true;
        }

        return $this;
    }

    /**
     * Set the value of [nom] column.
     *
     * @param string $v New value
     * @return $this The current object (for fluent API support)
     */
    public function setNom($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->nom !== $v) {
            $this->nom = $v;
            $this->modifiedColumns[UtilisateurTableMap::COL_NOM] = true;
        }

        return $this;
    }

    /**
     * Set the value of [prenom] column.
     *
     * @param string $v New value
     * @return $this The current object (for fluent API support)
     */
    public function setPrenom($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->prenom !== $v) {
            $this->prenom = $v;
            $this->modifiedColumns[UtilisateurTableMap::COL_PRENOM] = true;
        }

        return $this;
    }

    /**
     * Set the value of [login] column.
     *
     * @param string $v New value
     * @return $this The current object (for fluent API support)
     */
    public function setLogin($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->login !== $v) {
            $this->login = $v;
            $this->modifiedColumns[UtilisateurTableMap::COL_LOGIN] = true;
        }

        return $this;
    }

    /**
     * Set the value of [mdp] column.
     *
     * @param string $v New value
     * @return $this The current object (for fluent API support)
     */
    public function setMdp($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->mdp !== $v) {
            $this->mdp = $v;
            $this->modifiedColumns[UtilisateurTableMap::COL_MDP] = true;
        }

        return $this;
    }

    /**
     * Set the value of [adresse] column.
     *
     * @param string $v New value
     * @return $this The current object (for fluent API support)
     */
    public function setAdresse($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->adresse !== $v) {
            $this->adresse = $v;
            $this->modifiedColumns[UtilisateurTableMap::COL_ADRESSE] = true;
        }

        return $this;
    }

    /**
     * Set the value of [ville] column.
     *
     * @param string $v New value
     * @return $this The current object (for fluent API support)
     */
    public function setVille($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->ville !== $v) {
            $this->ville = $v;
            $this->modifiedColumns[UtilisateurTableMap::COL_VILLE] = true;
        }

        return $this;
    }

    /**
     * Set the value of [cp] column.
     *
     * @param string $v New value
     * @return $this The current object (for fluent API support)
     */
    public function setCp($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->cp !== $v) {
            $this->cp = $v;
            $this->modifiedColumns[UtilisateurTableMap::COL_CP] = true;
        }

        return $this;
    }

    /**
     * Sets the value of [dateembauche] column to a normalized version of the date/time value specified.
     *
     * @param string|integer|\DateTimeInterface $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this The current object (for fluent API support)
     */
    public function setDateembauche($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->dateembauche !== null || $dt !== null) {
            if ($this->dateembauche === null || $dt === null || $dt->format("Y-m-d") !== $this->dateembauche->format("Y-m-d")) {
                $this->dateembauche = $dt === null ? null : clone $dt;
                $this->modifiedColumns[UtilisateurTableMap::COL_DATEEMBAUCHE] = true;
            }
        } // if either are not null

        return $this;
    }

    /**
     * Set the value of [profil] column.
     *
     * @param string $v New value
     * @return $this The current object (for fluent API support)
     */
    public function setProfil($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->profil !== $v) {
            $this->profil = $v;
            $this->modifiedColumns[UtilisateurTableMap::COL_PROFIL] = true;
        }

        return $this;
    }

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return bool Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues(): bool
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    }

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by DataFetcher->fetch().
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param bool $rehydrate Whether this object is being re-hydrated from the database.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int next starting column
     * @throws \Propel\Runtime\Exception\PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate(array $row, int $startcol = 0, bool $rehydrate = false, string $indexType = TableMap::TYPE_NUM): int
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : UtilisateurTableMap::translateFieldName('Iduser', TableMap::TYPE_PHPNAME, $indexType)];
            $this->iduser = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : UtilisateurTableMap::translateFieldName('Nom', TableMap::TYPE_PHPNAME, $indexType)];
            $this->nom = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : UtilisateurTableMap::translateFieldName('Prenom', TableMap::TYPE_PHPNAME, $indexType)];
            $this->prenom = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : UtilisateurTableMap::translateFieldName('Login', TableMap::TYPE_PHPNAME, $indexType)];
            $this->login = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : UtilisateurTableMap::translateFieldName('Mdp', TableMap::TYPE_PHPNAME, $indexType)];
            $this->mdp = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : UtilisateurTableMap::translateFieldName('Adresse', TableMap::TYPE_PHPNAME, $indexType)];
            $this->adresse = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : UtilisateurTableMap::translateFieldName('Ville', TableMap::TYPE_PHPNAME, $indexType)];
            $this->ville = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : UtilisateurTableMap::translateFieldName('Cp', TableMap::TYPE_PHPNAME, $indexType)];
            $this->cp = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : UtilisateurTableMap::translateFieldName('Dateembauche', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00') {
                $col = null;
            }
            $this->dateembauche = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : UtilisateurTableMap::translateFieldName('Profil', TableMap::TYPE_PHPNAME, $indexType)];
            $this->profil = (null !== $col) ? (string) $col : null;

            $this->resetModified();
            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 10; // 10 = UtilisateurTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\App\\Http\\Model\\Utilisateur'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws \Propel\Runtime\Exception\PropelException
     * @return void
     */
    public function ensureConsistency(): void
    {
    }

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param bool $deep (optional) Whether to also de-associated any related objects.
     * @param ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws \Propel\Runtime\Exception\PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload(bool $deep = false, ?ConnectionInterface $con = null): void
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(UtilisateurTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildUtilisateurQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->collFichefraiss = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param ConnectionInterface $con
     * @return void
     * @throws \Propel\Runtime\Exception\PropelException
     * @see Utilisateur::setDeleted()
     * @see Utilisateur::isDeleted()
     */
    public function delete(?ConnectionInterface $con = null): void
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(UtilisateurTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildUtilisateurQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param ConnectionInterface $con
     * @return int The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws \Propel\Runtime\Exception\PropelException
     * @see doSave()
     */
    public function save(?ConnectionInterface $con = null): int
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(UtilisateurTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                UtilisateurTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param ConnectionInterface $con
     * @return int The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws \Propel\Runtime\Exception\PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con): int
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->fichefraissScheduledForDeletion !== null) {
                if (!$this->fichefraissScheduledForDeletion->isEmpty()) {
                    \App\Http\Model\FichefraisQuery::create()
                        ->filterByPrimaryKeys($this->fichefraissScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->fichefraissScheduledForDeletion = null;
                }
            }

            if ($this->collFichefraiss !== null) {
                foreach ($this->collFichefraiss as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    }

    /**
     * Insert the row in the database.
     *
     * @param ConnectionInterface $con
     *
     * @throws \Propel\Runtime\Exception\PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con): void
    {
        $modifiedColumns = [];
        $index = 0;


         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(UtilisateurTableMap::COL_IDUSER)) {
            $modifiedColumns[':p' . $index++]  = 'idUser';
        }
        if ($this->isColumnModified(UtilisateurTableMap::COL_NOM)) {
            $modifiedColumns[':p' . $index++]  = 'nom';
        }
        if ($this->isColumnModified(UtilisateurTableMap::COL_PRENOM)) {
            $modifiedColumns[':p' . $index++]  = 'prenom';
        }
        if ($this->isColumnModified(UtilisateurTableMap::COL_LOGIN)) {
            $modifiedColumns[':p' . $index++]  = 'login';
        }
        if ($this->isColumnModified(UtilisateurTableMap::COL_MDP)) {
            $modifiedColumns[':p' . $index++]  = 'mdp';
        }
        if ($this->isColumnModified(UtilisateurTableMap::COL_ADRESSE)) {
            $modifiedColumns[':p' . $index++]  = 'adresse';
        }
        if ($this->isColumnModified(UtilisateurTableMap::COL_VILLE)) {
            $modifiedColumns[':p' . $index++]  = 'ville';
        }
        if ($this->isColumnModified(UtilisateurTableMap::COL_CP)) {
            $modifiedColumns[':p' . $index++]  = 'cp';
        }
        if ($this->isColumnModified(UtilisateurTableMap::COL_DATEEMBAUCHE)) {
            $modifiedColumns[':p' . $index++]  = 'dateEmbauche';
        }
        if ($this->isColumnModified(UtilisateurTableMap::COL_PROFIL)) {
            $modifiedColumns[':p' . $index++]  = 'profil';
        }

        $sql = sprintf(
            'INSERT INTO utilisateur (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'idUser':
                        $stmt->bindValue($identifier, $this->iduser, PDO::PARAM_STR);

                        break;
                    case 'nom':
                        $stmt->bindValue($identifier, $this->nom, PDO::PARAM_STR);

                        break;
                    case 'prenom':
                        $stmt->bindValue($identifier, $this->prenom, PDO::PARAM_STR);

                        break;
                    case 'login':
                        $stmt->bindValue($identifier, $this->login, PDO::PARAM_STR);

                        break;
                    case 'mdp':
                        $stmt->bindValue($identifier, $this->mdp, PDO::PARAM_STR);

                        break;
                    case 'adresse':
                        $stmt->bindValue($identifier, $this->adresse, PDO::PARAM_STR);

                        break;
                    case 'ville':
                        $stmt->bindValue($identifier, $this->ville, PDO::PARAM_STR);

                        break;
                    case 'cp':
                        $stmt->bindValue($identifier, $this->cp, PDO::PARAM_STR);

                        break;
                    case 'dateEmbauche':
                        $stmt->bindValue($identifier, $this->dateembauche ? $this->dateembauche->format("Y-m-d") : null, PDO::PARAM_STR);

                        break;
                    case 'profil':
                        $stmt->bindValue($identifier, $this->profil, PDO::PARAM_STR);

                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param ConnectionInterface $con
     *
     * @return int Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con): int
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName(string $name, string $type = TableMap::TYPE_PHPNAME)
    {
        $pos = UtilisateurTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos Position in XML schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition(int $pos)
    {
        switch ($pos) {
            case 0:
                return $this->getIduser();

            case 1:
                return $this->getNom();

            case 2:
                return $this->getPrenom();

            case 3:
                return $this->getLogin();

            case 4:
                return $this->getMdp();

            case 5:
                return $this->getAdresse();

            case 6:
                return $this->getVille();

            case 7:
                return $this->getCp();

            case 8:
                return $this->getDateembauche();

            case 9:
                return $this->getProfil();

            default:
                return null;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param string $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param bool $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param bool $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array An associative array containing the field names (as keys) and field values
     */
    public function toArray(string $keyType = TableMap::TYPE_PHPNAME, bool $includeLazyLoadColumns = true, array $alreadyDumpedObjects = [], bool $includeForeignObjects = false): array
    {
        if (isset($alreadyDumpedObjects['Utilisateur'][$this->hashCode()])) {
            return ['*RECURSION*'];
        }
        $alreadyDumpedObjects['Utilisateur'][$this->hashCode()] = true;
        $keys = UtilisateurTableMap::getFieldNames($keyType);
        $result = [
            $keys[0] => $this->getIduser(),
            $keys[1] => $this->getNom(),
            $keys[2] => $this->getPrenom(),
            $keys[3] => $this->getLogin(),
            $keys[4] => $this->getMdp(),
            $keys[5] => $this->getAdresse(),
            $keys[6] => $this->getVille(),
            $keys[7] => $this->getCp(),
            $keys[8] => $this->getDateembauche(),
            $keys[9] => $this->getProfil(),
        ];
        if ($result[$keys[8]] instanceof \DateTimeInterface) {
            $result[$keys[8]] = $result[$keys[8]]->format('Y-m-d');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->collFichefraiss) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'fichefraiss';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'fichefraiss';
                        break;
                    default:
                        $key = 'Fichefraiss';
                }

                $result[$key] = $this->collFichefraiss->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this
     */
    public function setByName(string $name, $value, string $type = TableMap::TYPE_PHPNAME)
    {
        $pos = UtilisateurTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        $this->setByPosition($pos, $value);

        return $this;
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return $this
     */
    public function setByPosition(int $pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setIduser($value);
                break;
            case 1:
                $this->setNom($value);
                break;
            case 2:
                $this->setPrenom($value);
                break;
            case 3:
                $this->setLogin($value);
                break;
            case 4:
                $this->setMdp($value);
                break;
            case 5:
                $this->setAdresse($value);
                break;
            case 6:
                $this->setVille($value);
                break;
            case 7:
                $this->setCp($value);
                break;
            case 8:
                $this->setDateembauche($value);
                break;
            case 9:
                $this->setProfil($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param array $arr An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return $this
     */
    public function fromArray(array $arr, string $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = UtilisateurTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setIduser($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setNom($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setPrenom($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setLogin($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setMdp($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setAdresse($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setVille($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setCp($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setDateembauche($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setProfil($arr[$keys[9]]);
        }

        return $this;
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this The current object, for fluid interface
     */
    public function importFrom($parser, string $data, string $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return \Propel\Runtime\ActiveQuery\Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria(): Criteria
    {
        $criteria = new Criteria(UtilisateurTableMap::DATABASE_NAME);

        if ($this->isColumnModified(UtilisateurTableMap::COL_IDUSER)) {
            $criteria->add(UtilisateurTableMap::COL_IDUSER, $this->iduser);
        }
        if ($this->isColumnModified(UtilisateurTableMap::COL_NOM)) {
            $criteria->add(UtilisateurTableMap::COL_NOM, $this->nom);
        }
        if ($this->isColumnModified(UtilisateurTableMap::COL_PRENOM)) {
            $criteria->add(UtilisateurTableMap::COL_PRENOM, $this->prenom);
        }
        if ($this->isColumnModified(UtilisateurTableMap::COL_LOGIN)) {
            $criteria->add(UtilisateurTableMap::COL_LOGIN, $this->login);
        }
        if ($this->isColumnModified(UtilisateurTableMap::COL_MDP)) {
            $criteria->add(UtilisateurTableMap::COL_MDP, $this->mdp);
        }
        if ($this->isColumnModified(UtilisateurTableMap::COL_ADRESSE)) {
            $criteria->add(UtilisateurTableMap::COL_ADRESSE, $this->adresse);
        }
        if ($this->isColumnModified(UtilisateurTableMap::COL_VILLE)) {
            $criteria->add(UtilisateurTableMap::COL_VILLE, $this->ville);
        }
        if ($this->isColumnModified(UtilisateurTableMap::COL_CP)) {
            $criteria->add(UtilisateurTableMap::COL_CP, $this->cp);
        }
        if ($this->isColumnModified(UtilisateurTableMap::COL_DATEEMBAUCHE)) {
            $criteria->add(UtilisateurTableMap::COL_DATEEMBAUCHE, $this->dateembauche);
        }
        if ($this->isColumnModified(UtilisateurTableMap::COL_PROFIL)) {
            $criteria->add(UtilisateurTableMap::COL_PROFIL, $this->profil);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return \Propel\Runtime\ActiveQuery\Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria(): Criteria
    {
        $criteria = ChildUtilisateurQuery::create();
        $criteria->add(UtilisateurTableMap::COL_IDUSER, $this->iduser);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int|string Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getIduser();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->getIduser();
    }

    /**
     * Generic method to set the primary key (iduser column).
     *
     * @param string|null $key Primary key.
     * @return void
     */
    public function setPrimaryKey(?string $key = null): void
    {
        $this->setIduser($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     *
     * @return bool
     */
    public function isPrimaryKeyNull(): bool
    {
        return null === $this->getIduser();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of \App\Http\Model\Utilisateur (or compatible) type.
     * @param bool $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param bool $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws \Propel\Runtime\Exception\PropelException
     * @return void
     */
    public function copyInto(object $copyObj, bool $deepCopy = false, bool $makeNew = true): void
    {
        $copyObj->setIduser($this->getIduser());
        $copyObj->setNom($this->getNom());
        $copyObj->setPrenom($this->getPrenom());
        $copyObj->setLogin($this->getLogin());
        $copyObj->setMdp($this->getMdp());
        $copyObj->setAdresse($this->getAdresse());
        $copyObj->setVille($this->getVille());
        $copyObj->setCp($this->getCp());
        $copyObj->setDateembauche($this->getDateembauche());
        $copyObj->setProfil($this->getProfil());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getFichefraiss() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addFichefrais($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param bool $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \App\Http\Model\Utilisateur Clone of current object.
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function copy(bool $deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName): void
    {
        if ('Fichefrais' === $relationName) {
            $this->initFichefraiss();
            return;
        }
    }

    /**
     * Clears out the collFichefraiss collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return $this
     * @see addFichefraiss()
     */
    public function clearFichefraiss()
    {
        $this->collFichefraiss = null; // important to set this to NULL since that means it is uninitialized

        return $this;
    }

    /**
     * Reset is the collFichefraiss collection loaded partially.
     *
     * @return void
     */
    public function resetPartialFichefraiss($v = true): void
    {
        $this->collFichefraissPartial = $v;
    }

    /**
     * Initializes the collFichefraiss collection.
     *
     * By default this just sets the collFichefraiss collection to an empty array (like clearcollFichefraiss());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param bool $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initFichefraiss(bool $overrideExisting = true): void
    {
        if (null !== $this->collFichefraiss && !$overrideExisting) {
            return;
        }

        $collectionClassName = FichefraisTableMap::getTableMap()->getCollectionClassName();

        $this->collFichefraiss = new $collectionClassName;
        $this->collFichefraiss->setModel('\App\Http\Model\Fichefrais');
    }

    /**
     * Gets an array of ChildFichefrais objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUtilisateur is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildFichefrais[] List of ChildFichefrais objects
     * @phpstan-return ObjectCollection&\Traversable<ChildFichefrais> List of ChildFichefrais objects
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function getFichefraiss(?Criteria $criteria = null, ?ConnectionInterface $con = null)
    {
        $partial = $this->collFichefraissPartial && !$this->isNew();
        if (null === $this->collFichefraiss || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collFichefraiss) {
                    $this->initFichefraiss();
                } else {
                    $collectionClassName = FichefraisTableMap::getTableMap()->getCollectionClassName();

                    $collFichefraiss = new $collectionClassName;
                    $collFichefraiss->setModel('\App\Http\Model\Fichefrais');

                    return $collFichefraiss;
                }
            } else {
                $collFichefraiss = ChildFichefraisQuery::create(null, $criteria)
                    ->filterByUtilisateur($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collFichefraissPartial && count($collFichefraiss)) {
                        $this->initFichefraiss(false);

                        foreach ($collFichefraiss as $obj) {
                            if (false == $this->collFichefraiss->contains($obj)) {
                                $this->collFichefraiss->append($obj);
                            }
                        }

                        $this->collFichefraissPartial = true;
                    }

                    return $collFichefraiss;
                }

                if ($partial && $this->collFichefraiss) {
                    foreach ($this->collFichefraiss as $obj) {
                        if ($obj->isNew()) {
                            $collFichefraiss[] = $obj;
                        }
                    }
                }

                $this->collFichefraiss = $collFichefraiss;
                $this->collFichefraissPartial = false;
            }
        }

        return $this->collFichefraiss;
    }

    /**
     * Sets a collection of ChildFichefrais objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param Collection $fichefraiss A Propel collection.
     * @param ConnectionInterface $con Optional connection object
     * @return $this The current object (for fluent API support)
     */
    public function setFichefraiss(Collection $fichefraiss, ?ConnectionInterface $con = null)
    {
        /** @var ChildFichefrais[] $fichefraissToDelete */
        $fichefraissToDelete = $this->getFichefraiss(new Criteria(), $con)->diff($fichefraiss);


        $this->fichefraissScheduledForDeletion = $fichefraissToDelete;

        foreach ($fichefraissToDelete as $fichefraisRemoved) {
            $fichefraisRemoved->setUtilisateur(null);
        }

        $this->collFichefraiss = null;
        foreach ($fichefraiss as $fichefrais) {
            $this->addFichefrais($fichefrais);
        }

        $this->collFichefraiss = $fichefraiss;
        $this->collFichefraissPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Fichefrais objects.
     *
     * @param Criteria $criteria
     * @param bool $distinct
     * @param ConnectionInterface $con
     * @return int Count of related Fichefrais objects.
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function countFichefraiss(?Criteria $criteria = null, bool $distinct = false, ?ConnectionInterface $con = null): int
    {
        $partial = $this->collFichefraissPartial && !$this->isNew();
        if (null === $this->collFichefraiss || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collFichefraiss) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getFichefraiss());
            }

            $query = ChildFichefraisQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUtilisateur($this)
                ->count($con);
        }

        return count($this->collFichefraiss);
    }

    /**
     * Method called to associate a ChildFichefrais object to this object
     * through the ChildFichefrais foreign key attribute.
     *
     * @param ChildFichefrais $l ChildFichefrais
     * @return $this The current object (for fluent API support)
     */
    public function addFichefrais(ChildFichefrais $l)
    {
        if ($this->collFichefraiss === null) {
            $this->initFichefraiss();
            $this->collFichefraissPartial = true;
        }

        if (!$this->collFichefraiss->contains($l)) {
            $this->doAddFichefrais($l);

            if ($this->fichefraissScheduledForDeletion and $this->fichefraissScheduledForDeletion->contains($l)) {
                $this->fichefraissScheduledForDeletion->remove($this->fichefraissScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildFichefrais $fichefrais The ChildFichefrais object to add.
     */
    protected function doAddFichefrais(ChildFichefrais $fichefrais): void
    {
        $this->collFichefraiss[]= $fichefrais;
        $fichefrais->setUtilisateur($this);
    }

    /**
     * @param ChildFichefrais $fichefrais The ChildFichefrais object to remove.
     * @return $this The current object (for fluent API support)
     */
    public function removeFichefrais(ChildFichefrais $fichefrais)
    {
        if ($this->getFichefraiss()->contains($fichefrais)) {
            $pos = $this->collFichefraiss->search($fichefrais);
            $this->collFichefraiss->remove($pos);
            if (null === $this->fichefraissScheduledForDeletion) {
                $this->fichefraissScheduledForDeletion = clone $this->collFichefraiss;
                $this->fichefraissScheduledForDeletion->clear();
            }
            $this->fichefraissScheduledForDeletion[]= clone $fichefrais;
            $fichefrais->setUtilisateur(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Utilisateur is new, it will return
     * an empty collection; or if this Utilisateur has previously
     * been saved, it will retrieve related Fichefraiss from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Utilisateur.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param ConnectionInterface $con optional connection object
     * @param string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildFichefrais[] List of ChildFichefrais objects
     * @phpstan-return ObjectCollection&\Traversable<ChildFichefrais}> List of ChildFichefrais objects
     */
    public function getFichefraissJoinEtat(?Criteria $criteria = null, ?ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildFichefraisQuery::create(null, $criteria);
        $query->joinWith('Etat', $joinBehavior);

        return $this->getFichefraiss($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     *
     * @return $this
     */
    public function clear()
    {
        $this->iduser = null;
        $this->nom = null;
        $this->prenom = null;
        $this->login = null;
        $this->mdp = null;
        $this->adresse = null;
        $this->ville = null;
        $this->cp = null;
        $this->dateembauche = null;
        $this->profil = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);

        return $this;
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param bool $deep Whether to also clear the references on all referrer objects.
     * @return $this
     */
    public function clearAllReferences(bool $deep = false)
    {
        if ($deep) {
            if ($this->collFichefraiss) {
                foreach ($this->collFichefraiss as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collFichefraiss = null;
        return $this;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(UtilisateurTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param ConnectionInterface|null $con
     * @return bool
     */
    public function preSave(?ConnectionInterface $con = null): bool
    {
                return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface|null $con
     * @return void
     */
    public function postSave(?ConnectionInterface $con = null): void
    {
            }

    /**
     * Code to be run before inserting to database
     * @param ConnectionInterface|null $con
     * @return bool
     */
    public function preInsert(?ConnectionInterface $con = null): bool
    {
                return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface|null $con
     * @return void
     */
    public function postInsert(?ConnectionInterface $con = null): void
    {
            }

    /**
     * Code to be run before updating the object in database
     * @param ConnectionInterface|null $con
     * @return bool
     */
    public function preUpdate(?ConnectionInterface $con = null): bool
    {
                return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface|null $con
     * @return void
     */
    public function postUpdate(?ConnectionInterface $con = null): void
    {
            }

    /**
     * Code to be run before deleting the object in database
     * @param ConnectionInterface|null $con
     * @return bool
     */
    public function preDelete(?ConnectionInterface $con = null): bool
    {
                return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface|null $con
     * @return void
     */
    public function postDelete(?ConnectionInterface $con = null): void
    {
            }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);
            $inputData = $params[0];
            $keyType = $params[1] ?? TableMap::TYPE_PHPNAME;

            return $this->importFrom($format, $inputData, $keyType);
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = $params[0] ?? true;
            $keyType = $params[1] ?? TableMap::TYPE_PHPNAME;

            return $this->exportTo($format, $includeLazyLoadColumns, $keyType);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
