<?php

namespace App\Http\Model\Base;

use \Exception;
use \PDO;
use App\Http\Model\Fichefrais as ChildFichefrais;
use App\Http\Model\FichefraisQuery as ChildFichefraisQuery;
use App\Http\Model\Map\FichefraisTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the `fichefrais` table.
 *
 * @method     ChildFichefraisQuery orderByIdfichefrais($order = Criteria::ASC) Order by the idFicheFrais column
 * @method     ChildFichefraisQuery orderByMoisannee($order = Criteria::ASC) Order by the moisAnnee column
 * @method     ChildFichefraisQuery orderByNbjustificatifs($order = Criteria::ASC) Order by the nbJustificatifs column
 * @method     ChildFichefraisQuery orderByMontantvalide($order = Criteria::ASC) Order by the montantValide column
 * @method     ChildFichefraisQuery orderByDatemodif($order = Criteria::ASC) Order by the dateModif column
 * @method     ChildFichefraisQuery orderByIdvisiteur($order = Criteria::ASC) Order by the idVisiteur column
 * @method     ChildFichefraisQuery orderByIdetat($order = Criteria::ASC) Order by the idEtat column
 *
 * @method     ChildFichefraisQuery groupByIdfichefrais() Group by the idFicheFrais column
 * @method     ChildFichefraisQuery groupByMoisannee() Group by the moisAnnee column
 * @method     ChildFichefraisQuery groupByNbjustificatifs() Group by the nbJustificatifs column
 * @method     ChildFichefraisQuery groupByMontantvalide() Group by the montantValide column
 * @method     ChildFichefraisQuery groupByDatemodif() Group by the dateModif column
 * @method     ChildFichefraisQuery groupByIdvisiteur() Group by the idVisiteur column
 * @method     ChildFichefraisQuery groupByIdetat() Group by the idEtat column
 *
 * @method     ChildFichefraisQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildFichefraisQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildFichefraisQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildFichefraisQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildFichefraisQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildFichefraisQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildFichefraisQuery leftJoinEtat($relationAlias = null) Adds a LEFT JOIN clause to the query using the Etat relation
 * @method     ChildFichefraisQuery rightJoinEtat($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Etat relation
 * @method     ChildFichefraisQuery innerJoinEtat($relationAlias = null) Adds a INNER JOIN clause to the query using the Etat relation
 *
 * @method     ChildFichefraisQuery joinWithEtat($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Etat relation
 *
 * @method     ChildFichefraisQuery leftJoinWithEtat() Adds a LEFT JOIN clause and with to the query using the Etat relation
 * @method     ChildFichefraisQuery rightJoinWithEtat() Adds a RIGHT JOIN clause and with to the query using the Etat relation
 * @method     ChildFichefraisQuery innerJoinWithEtat() Adds a INNER JOIN clause and with to the query using the Etat relation
 *
 * @method     ChildFichefraisQuery leftJoinUtilisateur($relationAlias = null) Adds a LEFT JOIN clause to the query using the Utilisateur relation
 * @method     ChildFichefraisQuery rightJoinUtilisateur($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Utilisateur relation
 * @method     ChildFichefraisQuery innerJoinUtilisateur($relationAlias = null) Adds a INNER JOIN clause to the query using the Utilisateur relation
 *
 * @method     ChildFichefraisQuery joinWithUtilisateur($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Utilisateur relation
 *
 * @method     ChildFichefraisQuery leftJoinWithUtilisateur() Adds a LEFT JOIN clause and with to the query using the Utilisateur relation
 * @method     ChildFichefraisQuery rightJoinWithUtilisateur() Adds a RIGHT JOIN clause and with to the query using the Utilisateur relation
 * @method     ChildFichefraisQuery innerJoinWithUtilisateur() Adds a INNER JOIN clause and with to the query using the Utilisateur relation
 *
 * @method     \App\Http\Model\EtatQuery|\App\Http\Model\UtilisateurQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildFichefrais|null findOne(?ConnectionInterface $con = null) Return the first ChildFichefrais matching the query
 * @method     ChildFichefrais findOneOrCreate(?ConnectionInterface $con = null) Return the first ChildFichefrais matching the query, or a new ChildFichefrais object populated from the query conditions when no match is found
 *
 * @method     ChildFichefrais|null findOneByIdfichefrais(int $idFicheFrais) Return the first ChildFichefrais filtered by the idFicheFrais column
 * @method     ChildFichefrais|null findOneByMoisannee(string $moisAnnee) Return the first ChildFichefrais filtered by the moisAnnee column
 * @method     ChildFichefrais|null findOneByNbjustificatifs(int $nbJustificatifs) Return the first ChildFichefrais filtered by the nbJustificatifs column
 * @method     ChildFichefrais|null findOneByMontantvalide(string $montantValide) Return the first ChildFichefrais filtered by the montantValide column
 * @method     ChildFichefrais|null findOneByDatemodif(int $dateModif) Return the first ChildFichefrais filtered by the dateModif column
 * @method     ChildFichefrais|null findOneByIdvisiteur(string $idVisiteur) Return the first ChildFichefrais filtered by the idVisiteur column
 * @method     ChildFichefrais|null findOneByIdetat(string $idEtat) Return the first ChildFichefrais filtered by the idEtat column
 *
 * @method     ChildFichefrais requirePk($key, ?ConnectionInterface $con = null) Return the ChildFichefrais by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFichefrais requireOne(?ConnectionInterface $con = null) Return the first ChildFichefrais matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildFichefrais requireOneByIdfichefrais(int $idFicheFrais) Return the first ChildFichefrais filtered by the idFicheFrais column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFichefrais requireOneByMoisannee(string $moisAnnee) Return the first ChildFichefrais filtered by the moisAnnee column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFichefrais requireOneByNbjustificatifs(int $nbJustificatifs) Return the first ChildFichefrais filtered by the nbJustificatifs column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFichefrais requireOneByMontantvalide(string $montantValide) Return the first ChildFichefrais filtered by the montantValide column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFichefrais requireOneByDatemodif(int $dateModif) Return the first ChildFichefrais filtered by the dateModif column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFichefrais requireOneByIdvisiteur(string $idVisiteur) Return the first ChildFichefrais filtered by the idVisiteur column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFichefrais requireOneByIdetat(string $idEtat) Return the first ChildFichefrais filtered by the idEtat column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildFichefrais[]|Collection find(?ConnectionInterface $con = null) Return ChildFichefrais objects based on current ModelCriteria
 * @psalm-method Collection&\Traversable<ChildFichefrais> find(?ConnectionInterface $con = null) Return ChildFichefrais objects based on current ModelCriteria
 *
 * @method     ChildFichefrais[]|Collection findByIdfichefrais(int|array<int> $idFicheFrais) Return ChildFichefrais objects filtered by the idFicheFrais column
 * @psalm-method Collection&\Traversable<ChildFichefrais> findByIdfichefrais(int|array<int> $idFicheFrais) Return ChildFichefrais objects filtered by the idFicheFrais column
 * @method     ChildFichefrais[]|Collection findByMoisannee(string|array<string> $moisAnnee) Return ChildFichefrais objects filtered by the moisAnnee column
 * @psalm-method Collection&\Traversable<ChildFichefrais> findByMoisannee(string|array<string> $moisAnnee) Return ChildFichefrais objects filtered by the moisAnnee column
 * @method     ChildFichefrais[]|Collection findByNbjustificatifs(int|array<int> $nbJustificatifs) Return ChildFichefrais objects filtered by the nbJustificatifs column
 * @psalm-method Collection&\Traversable<ChildFichefrais> findByNbjustificatifs(int|array<int> $nbJustificatifs) Return ChildFichefrais objects filtered by the nbJustificatifs column
 * @method     ChildFichefrais[]|Collection findByMontantvalide(string|array<string> $montantValide) Return ChildFichefrais objects filtered by the montantValide column
 * @psalm-method Collection&\Traversable<ChildFichefrais> findByMontantvalide(string|array<string> $montantValide) Return ChildFichefrais objects filtered by the montantValide column
 * @method     ChildFichefrais[]|Collection findByDatemodif(int|array<int> $dateModif) Return ChildFichefrais objects filtered by the dateModif column
 * @psalm-method Collection&\Traversable<ChildFichefrais> findByDatemodif(int|array<int> $dateModif) Return ChildFichefrais objects filtered by the dateModif column
 * @method     ChildFichefrais[]|Collection findByIdvisiteur(string|array<string> $idVisiteur) Return ChildFichefrais objects filtered by the idVisiteur column
 * @psalm-method Collection&\Traversable<ChildFichefrais> findByIdvisiteur(string|array<string> $idVisiteur) Return ChildFichefrais objects filtered by the idVisiteur column
 * @method     ChildFichefrais[]|Collection findByIdetat(string|array<string> $idEtat) Return ChildFichefrais objects filtered by the idEtat column
 * @psalm-method Collection&\Traversable<ChildFichefrais> findByIdetat(string|array<string> $idEtat) Return ChildFichefrais objects filtered by the idEtat column
 *
 * @method     ChildFichefrais[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ?ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 * @psalm-method \Propel\Runtime\Util\PropelModelPager&\Traversable<ChildFichefrais> paginate($page = 1, $maxPerPage = 10, ?ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 */
abstract class FichefraisQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \App\Http\Model\Base\FichefraisQuery object.
     *
     * @param string $dbName The database name
     * @param string $modelName The phpName of a model, e.g. 'Book'
     * @param string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'ws-gsb-connection', $modelName = '\\App\\Http\\Model\\Fichefrais', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildFichefraisQuery object.
     *
     * @param string $modelAlias The alias of a model in the query
     * @param Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildFichefraisQuery
     */
    public static function create(?string $modelAlias = null, ?Criteria $criteria = null): Criteria
    {
        if ($criteria instanceof ChildFichefraisQuery) {
            return $criteria;
        }
        $query = new ChildFichefraisQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildFichefrais|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ?ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(FichefraisTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = FichefraisTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildFichefrais A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT idFicheFrais, moisAnnee, nbJustificatifs, montantValide, dateModif, idVisiteur, idEtat FROM fichefrais WHERE idFicheFrais = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildFichefrais $obj */
            $obj = new ChildFichefrais();
            $obj->hydrate($row);
            FichefraisTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con A connection object
     *
     * @return ChildFichefrais|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param array $keys Primary keys to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return Collection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ?ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param mixed $key Primary key to use for the query
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        $this->addUsingAlias(FichefraisTableMap::COL_IDFICHEFRAIS, $key, Criteria::EQUAL);

        return $this;
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param array|int $keys The list of primary key to use for the query
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        $this->addUsingAlias(FichefraisTableMap::COL_IDFICHEFRAIS, $keys, Criteria::IN);

        return $this;
    }

    /**
     * Filter the query on the idFicheFrais column
     *
     * Example usage:
     * <code>
     * $query->filterByIdfichefrais(1234); // WHERE idFicheFrais = 1234
     * $query->filterByIdfichefrais(array(12, 34)); // WHERE idFicheFrais IN (12, 34)
     * $query->filterByIdfichefrais(array('min' => 12)); // WHERE idFicheFrais > 12
     * </code>
     *
     * @param mixed $idfichefrais The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByIdfichefrais($idfichefrais = null, ?string $comparison = null)
    {
        if (is_array($idfichefrais)) {
            $useMinMax = false;
            if (isset($idfichefrais['min'])) {
                $this->addUsingAlias(FichefraisTableMap::COL_IDFICHEFRAIS, $idfichefrais['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($idfichefrais['max'])) {
                $this->addUsingAlias(FichefraisTableMap::COL_IDFICHEFRAIS, $idfichefrais['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(FichefraisTableMap::COL_IDFICHEFRAIS, $idfichefrais, $comparison);

        return $this;
    }

    /**
     * Filter the query on the moisAnnee column
     *
     * Example usage:
     * <code>
     * $query->filterByMoisannee('fooValue');   // WHERE moisAnnee = 'fooValue'
     * $query->filterByMoisannee('%fooValue%', Criteria::LIKE); // WHERE moisAnnee LIKE '%fooValue%'
     * $query->filterByMoisannee(['foo', 'bar']); // WHERE moisAnnee IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $moisannee The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByMoisannee($moisannee = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($moisannee)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(FichefraisTableMap::COL_MOISANNEE, $moisannee, $comparison);

        return $this;
    }

    /**
     * Filter the query on the nbJustificatifs column
     *
     * Example usage:
     * <code>
     * $query->filterByNbjustificatifs(1234); // WHERE nbJustificatifs = 1234
     * $query->filterByNbjustificatifs(array(12, 34)); // WHERE nbJustificatifs IN (12, 34)
     * $query->filterByNbjustificatifs(array('min' => 12)); // WHERE nbJustificatifs > 12
     * </code>
     *
     * @param mixed $nbjustificatifs The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByNbjustificatifs($nbjustificatifs = null, ?string $comparison = null)
    {
        if (is_array($nbjustificatifs)) {
            $useMinMax = false;
            if (isset($nbjustificatifs['min'])) {
                $this->addUsingAlias(FichefraisTableMap::COL_NBJUSTIFICATIFS, $nbjustificatifs['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($nbjustificatifs['max'])) {
                $this->addUsingAlias(FichefraisTableMap::COL_NBJUSTIFICATIFS, $nbjustificatifs['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(FichefraisTableMap::COL_NBJUSTIFICATIFS, $nbjustificatifs, $comparison);

        return $this;
    }

    /**
     * Filter the query on the montantValide column
     *
     * Example usage:
     * <code>
     * $query->filterByMontantvalide(1234); // WHERE montantValide = 1234
     * $query->filterByMontantvalide(array(12, 34)); // WHERE montantValide IN (12, 34)
     * $query->filterByMontantvalide(array('min' => 12)); // WHERE montantValide > 12
     * </code>
     *
     * @param mixed $montantvalide The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByMontantvalide($montantvalide = null, ?string $comparison = null)
    {
        if (is_array($montantvalide)) {
            $useMinMax = false;
            if (isset($montantvalide['min'])) {
                $this->addUsingAlias(FichefraisTableMap::COL_MONTANTVALIDE, $montantvalide['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($montantvalide['max'])) {
                $this->addUsingAlias(FichefraisTableMap::COL_MONTANTVALIDE, $montantvalide['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(FichefraisTableMap::COL_MONTANTVALIDE, $montantvalide, $comparison);

        return $this;
    }

    /**
     * Filter the query on the dateModif column
     *
     * Example usage:
     * <code>
     * $query->filterByDatemodif(1234); // WHERE dateModif = 1234
     * $query->filterByDatemodif(array(12, 34)); // WHERE dateModif IN (12, 34)
     * $query->filterByDatemodif(array('min' => 12)); // WHERE dateModif > 12
     * </code>
     *
     * @param mixed $datemodif The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByDatemodif($datemodif = null, ?string $comparison = null)
    {
        if (is_array($datemodif)) {
            $useMinMax = false;
            if (isset($datemodif['min'])) {
                $this->addUsingAlias(FichefraisTableMap::COL_DATEMODIF, $datemodif['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($datemodif['max'])) {
                $this->addUsingAlias(FichefraisTableMap::COL_DATEMODIF, $datemodif['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(FichefraisTableMap::COL_DATEMODIF, $datemodif, $comparison);

        return $this;
    }

    /**
     * Filter the query on the idVisiteur column
     *
     * Example usage:
     * <code>
     * $query->filterByIdvisiteur('fooValue');   // WHERE idVisiteur = 'fooValue'
     * $query->filterByIdvisiteur('%fooValue%', Criteria::LIKE); // WHERE idVisiteur LIKE '%fooValue%'
     * $query->filterByIdvisiteur(['foo', 'bar']); // WHERE idVisiteur IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $idvisiteur The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByIdvisiteur($idvisiteur = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($idvisiteur)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(FichefraisTableMap::COL_IDVISITEUR, $idvisiteur, $comparison);

        return $this;
    }

    /**
     * Filter the query on the idEtat column
     *
     * Example usage:
     * <code>
     * $query->filterByIdetat('fooValue');   // WHERE idEtat = 'fooValue'
     * $query->filterByIdetat('%fooValue%', Criteria::LIKE); // WHERE idEtat LIKE '%fooValue%'
     * $query->filterByIdetat(['foo', 'bar']); // WHERE idEtat IN ('foo', 'bar')
     * </code>
     *
     * @param string|string[] $idetat The value to use as filter.
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByIdetat($idetat = null, ?string $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($idetat)) {
                $comparison = Criteria::IN;
            }
        }

        $this->addUsingAlias(FichefraisTableMap::COL_IDETAT, $idetat, $comparison);

        return $this;
    }

    /**
     * Filter the query by a related \App\Http\Model\Etat object
     *
     * @param \App\Http\Model\Etat|ObjectCollection $etat The related object(s) to use as filter
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByEtat($etat, ?string $comparison = null)
    {
        if ($etat instanceof \App\Http\Model\Etat) {
            return $this
                ->addUsingAlias(FichefraisTableMap::COL_IDETAT, $etat->getIdetat(), $comparison);
        } elseif ($etat instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            $this
                ->addUsingAlias(FichefraisTableMap::COL_IDETAT, $etat->toKeyValue('PrimaryKey', 'Idetat'), $comparison);

            return $this;
        } else {
            throw new PropelException('filterByEtat() only accepts arguments of type \App\Http\Model\Etat or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Etat relation
     *
     * @param string|null $relationAlias Optional alias for the relation
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this The current query, for fluid interface
     */
    public function joinEtat(?string $relationAlias = null, ?string $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Etat');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Etat');
        }

        return $this;
    }

    /**
     * Use the Etat relation Etat object
     *
     * @see useQuery()
     *
     * @param string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \App\Http\Model\EtatQuery A secondary query class using the current class as primary query
     */
    public function useEtatQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinEtat($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Etat', '\App\Http\Model\EtatQuery');
    }

    /**
     * Use the Etat relation Etat object
     *
     * @param callable(\App\Http\Model\EtatQuery):\App\Http\Model\EtatQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withEtatQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useEtatQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }

    /**
     * Use the relation to Etat table for an EXISTS query.
     *
     * @see \Propel\Runtime\ActiveQuery\ModelCriteria::useExistsQuery()
     *
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     * @param string $typeOfExists Either ExistsQueryCriterion::TYPE_EXISTS or ExistsQueryCriterion::TYPE_NOT_EXISTS
     *
     * @return \App\Http\Model\EtatQuery The inner query object of the EXISTS statement
     */
    public function useEtatExistsQuery($modelAlias = null, $queryClass = null, $typeOfExists = 'EXISTS')
    {
        /** @var $q \App\Http\Model\EtatQuery */
        $q = $this->useExistsQuery('Etat', $modelAlias, $queryClass, $typeOfExists);
        return $q;
    }

    /**
     * Use the relation to Etat table for a NOT EXISTS query.
     *
     * @see useEtatExistsQuery()
     *
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     *
     * @return \App\Http\Model\EtatQuery The inner query object of the NOT EXISTS statement
     */
    public function useEtatNotExistsQuery($modelAlias = null, $queryClass = null)
    {
        /** @var $q \App\Http\Model\EtatQuery */
        $q = $this->useExistsQuery('Etat', $modelAlias, $queryClass, 'NOT EXISTS');
        return $q;
    }

    /**
     * Use the relation to Etat table for an IN query.
     *
     * @see \Propel\Runtime\ActiveQuery\ModelCriteria::useInQuery()
     *
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string|null $queryClass Allows to use a custom query class for the IN query, like ExtendedBookQuery::class
     * @param string $typeOfIn Criteria::IN or Criteria::NOT_IN
     *
     * @return \App\Http\Model\EtatQuery The inner query object of the IN statement
     */
    public function useInEtatQuery($modelAlias = null, $queryClass = null, $typeOfIn = 'IN')
    {
        /** @var $q \App\Http\Model\EtatQuery */
        $q = $this->useInQuery('Etat', $modelAlias, $queryClass, $typeOfIn);
        return $q;
    }

    /**
     * Use the relation to Etat table for a NOT IN query.
     *
     * @see useEtatInQuery()
     *
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string|null $queryClass Allows to use a custom query class for the NOT IN query, like ExtendedBookQuery::class
     *
     * @return \App\Http\Model\EtatQuery The inner query object of the NOT IN statement
     */
    public function useNotInEtatQuery($modelAlias = null, $queryClass = null)
    {
        /** @var $q \App\Http\Model\EtatQuery */
        $q = $this->useInQuery('Etat', $modelAlias, $queryClass, 'NOT IN');
        return $q;
    }

    /**
     * Filter the query by a related \App\Http\Model\Utilisateur object
     *
     * @param \App\Http\Model\Utilisateur|ObjectCollection $utilisateur The related object(s) to use as filter
     * @param string|null $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return $this The current query, for fluid interface
     */
    public function filterByUtilisateur($utilisateur, ?string $comparison = null)
    {
        if ($utilisateur instanceof \App\Http\Model\Utilisateur) {
            return $this
                ->addUsingAlias(FichefraisTableMap::COL_IDVISITEUR, $utilisateur->getIduser(), $comparison);
        } elseif ($utilisateur instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            $this
                ->addUsingAlias(FichefraisTableMap::COL_IDVISITEUR, $utilisateur->toKeyValue('PrimaryKey', 'Iduser'), $comparison);

            return $this;
        } else {
            throw new PropelException('filterByUtilisateur() only accepts arguments of type \App\Http\Model\Utilisateur or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Utilisateur relation
     *
     * @param string|null $relationAlias Optional alias for the relation
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this The current query, for fluid interface
     */
    public function joinUtilisateur(?string $relationAlias = null, ?string $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Utilisateur');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Utilisateur');
        }

        return $this;
    }

    /**
     * Use the Utilisateur relation Utilisateur object
     *
     * @see useQuery()
     *
     * @param string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \App\Http\Model\UtilisateurQuery A secondary query class using the current class as primary query
     */
    public function useUtilisateurQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUtilisateur($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Utilisateur', '\App\Http\Model\UtilisateurQuery');
    }

    /**
     * Use the Utilisateur relation Utilisateur object
     *
     * @param callable(\App\Http\Model\UtilisateurQuery):\App\Http\Model\UtilisateurQuery $callable A function working on the related query
     *
     * @param string|null $relationAlias optional alias for the relation
     *
     * @param string|null $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this
     */
    public function withUtilisateurQuery(
        callable $callable,
        string $relationAlias = null,
        ?string $joinType = Criteria::INNER_JOIN
    ) {
        $relatedQuery = $this->useUtilisateurQuery(
            $relationAlias,
            $joinType
        );
        $callable($relatedQuery);
        $relatedQuery->endUse();

        return $this;
    }

    /**
     * Use the relation to Utilisateur table for an EXISTS query.
     *
     * @see \Propel\Runtime\ActiveQuery\ModelCriteria::useExistsQuery()
     *
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     * @param string $typeOfExists Either ExistsQueryCriterion::TYPE_EXISTS or ExistsQueryCriterion::TYPE_NOT_EXISTS
     *
     * @return \App\Http\Model\UtilisateurQuery The inner query object of the EXISTS statement
     */
    public function useUtilisateurExistsQuery($modelAlias = null, $queryClass = null, $typeOfExists = 'EXISTS')
    {
        /** @var $q \App\Http\Model\UtilisateurQuery */
        $q = $this->useExistsQuery('Utilisateur', $modelAlias, $queryClass, $typeOfExists);
        return $q;
    }

    /**
     * Use the relation to Utilisateur table for a NOT EXISTS query.
     *
     * @see useUtilisateurExistsQuery()
     *
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string|null $queryClass Allows to use a custom query class for the exists query, like ExtendedBookQuery::class
     *
     * @return \App\Http\Model\UtilisateurQuery The inner query object of the NOT EXISTS statement
     */
    public function useUtilisateurNotExistsQuery($modelAlias = null, $queryClass = null)
    {
        /** @var $q \App\Http\Model\UtilisateurQuery */
        $q = $this->useExistsQuery('Utilisateur', $modelAlias, $queryClass, 'NOT EXISTS');
        return $q;
    }

    /**
     * Use the relation to Utilisateur table for an IN query.
     *
     * @see \Propel\Runtime\ActiveQuery\ModelCriteria::useInQuery()
     *
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string|null $queryClass Allows to use a custom query class for the IN query, like ExtendedBookQuery::class
     * @param string $typeOfIn Criteria::IN or Criteria::NOT_IN
     *
     * @return \App\Http\Model\UtilisateurQuery The inner query object of the IN statement
     */
    public function useInUtilisateurQuery($modelAlias = null, $queryClass = null, $typeOfIn = 'IN')
    {
        /** @var $q \App\Http\Model\UtilisateurQuery */
        $q = $this->useInQuery('Utilisateur', $modelAlias, $queryClass, $typeOfIn);
        return $q;
    }

    /**
     * Use the relation to Utilisateur table for a NOT IN query.
     *
     * @see useUtilisateurInQuery()
     *
     * @param string|null $modelAlias sets an alias for the nested query
     * @param string|null $queryClass Allows to use a custom query class for the NOT IN query, like ExtendedBookQuery::class
     *
     * @return \App\Http\Model\UtilisateurQuery The inner query object of the NOT IN statement
     */
    public function useNotInUtilisateurQuery($modelAlias = null, $queryClass = null)
    {
        /** @var $q \App\Http\Model\UtilisateurQuery */
        $q = $this->useInQuery('Utilisateur', $modelAlias, $queryClass, 'NOT IN');
        return $q;
    }

    /**
     * Exclude object from result
     *
     * @param ChildFichefrais $fichefrais Object to remove from the list of results
     *
     * @return $this The current query, for fluid interface
     */
    public function prune($fichefrais = null)
    {
        if ($fichefrais) {
            $this->addUsingAlias(FichefraisTableMap::COL_IDFICHEFRAIS, $fichefrais->getIdfichefrais(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the fichefrais table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(?ConnectionInterface $con = null): int
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(FichefraisTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            FichefraisTableMap::clearInstancePool();
            FichefraisTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws \Propel\Runtime\Exception\PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(?ConnectionInterface $con = null): int
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(FichefraisTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(FichefraisTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            FichefraisTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            FichefraisTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

}
