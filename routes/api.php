<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/test', function () {
    return 'L\'accès à l\'application est opérationnel';
});

Route::post('login', 'App\Http\Controllers\LoginController@authenticate')->name('Authenticate');



Route::resource('etat','App\Http\Controllers\EtatController')
        ->middleware('verify.propel.authentification');
Route::resource('fichefrais','App\Http\Controllers\FicheFraisController')
        ->middleware('verify.propel.authentification');
Route::resource('fraisforfait','App\Http\Controllers\FraisForfaitController')
        ->middleware('verify.propel.authentification');
Route::resource('lignefraisforfait','App\Http\Controllers\LigneFraisForfaitController')
        ->middleware('verify.propel.authentification');
Route::resource('lignefraishorsforfait','App\Http\Controllers\LigneFraisHorsForfaitController')
        ->middleware('verify.propel.authentification');


Route::fallback(function(){
    return response()->json([
        'message' => 'Page non trouvée. URL incorrecte. Consultez la documentation' ], 404);
});


