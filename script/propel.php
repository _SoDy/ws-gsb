<?php

return [
    'propel' => [
        'database' => [
            'connections' => [
                'ws-gsb-connection' => [
                    'adapter' => 'mysql',
                    'classname' => 'Propel\Runtime\Connection\ConnectionWrapper',
                    'dsn' => 'mysql:host=service_mysql;dbname=gsb',
                    'user' => 'root',
                    'password' => 'Azerty1',
                    'attributes' => []
                ],
            ]
        ],
        'general' => [
            'project' => 'ws-gsb.com'
        ],
        'paths' => [
            'projectDir' => '/var/www/html/ws-gsb.com',

            'schemaDir' => '/var/www/html/ws-gsb.com/script',
            'phpDir' => '/var/www/html/ws-gsb.com/app/Http/Model'
        ],
        'runtime' => [
            'defaultConnection' => 'ws-gsb-connection',
            'connections' => ['ws-gsb-connection']
        ],
        'generator' => [
            'defaultConnection' => 'ws-gsb-connection',
            'connections' => ['ws-gsb-connection']
        ]
    ]
]; 


