<?php
$serviceContainer = \Propel\Runtime\Propel::getServiceContainer();
$serviceContainer->initDatabaseMapFromDumps(array (
  'ws-gsb-connection' => 
  array (
    'tablesByName' => 
    array (
      'etat' => '\\App\\Http\\Model\\Map\\EtatTableMap',
      'fichefrais' => '\\App\\Http\\Model\\Map\\FichefraisTableMap',
      'fraisforfait' => '\\App\\Http\\Model\\Map\\FraisforfaitTableMap',
      'lignefraisforfait' => '\\App\\Http\\Model\\Map\\LignefraisforfaitTableMap',
      'lignefraishorsforfait' => '\\App\\Http\\Model\\Map\\LignefraishorsforfaitTableMap',
      'utilisateur' => '\\App\\Http\\Model\\Map\\UtilisateurTableMap',
    ),
    'tablesByPhpName' => 
    array (
      '\\Etat' => '\\App\\Http\\Model\\Map\\EtatTableMap',
      '\\Fichefrais' => '\\App\\Http\\Model\\Map\\FichefraisTableMap',
      '\\Fraisforfait' => '\\App\\Http\\Model\\Map\\FraisforfaitTableMap',
      '\\Lignefraisforfait' => '\\App\\Http\\Model\\Map\\LignefraisforfaitTableMap',
      '\\Lignefraishorsforfait' => '\\App\\Http\\Model\\Map\\LignefraishorsforfaitTableMap',
      '\\Utilisateur' => '\\App\\Http\\Model\\Map\\UtilisateurTableMap',
    ),
  ),
));
