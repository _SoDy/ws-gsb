-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Hôte : service_mysql
-- Généré le : ven. 21 juil. 2023 à 14:03
-- Version du serveur : 8.0.32
-- Version de PHP : 8.1.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `gsb`
--
CREATE DATABASE IF NOT EXISTS `gsb` DEFAULT CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci;
USE `gsb`;

-- --------------------------------------------------------

--
-- Structure de la table `etat`
--

CREATE TABLE `etat` (
  `idEtat` varchar(2) NOT NULL,
  `libelleEtat` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Déchargement des données de la table `etat`
--

INSERT INTO `etat` (`idEtat`, `libelleEtat`) VALUES
('EC', 'Fiche en cours de saisie'),
('ET', 'Clôturée. En traitement.'),
('MP', 'En paiement'),
('VA', 'Validé');

-- --------------------------------------------------------

--
-- Structure de la table `fichefrais`
--

CREATE TABLE `fichefrais` (
  `idFicheFrais` int NOT NULL,
  `moisAnnee` char(6) NOT NULL,
  `nbJustificatifs` int DEFAULT NULL,
  `montantValide` decimal(10,0) DEFAULT NULL,
  `dateModif` int DEFAULT NULL,
  `idVisiteur` varchar(4) NOT NULL,
  `idEtat` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Déchargement des données de la table `fichefrais`
--

INSERT INTO `fichefrais` (`idFicheFrais`, `moisAnnee`, `nbJustificatifs`, `montantValide`, `dateModif`, `idVisiteur`, `idEtat`) VALUES
(1, '042016', NULL, NULL, NULL, 'a55', 'EC'),
(8, '042023', NULL, NULL, NULL, 'a17', 'EC'),
(9, '052023', NULL, NULL, NULL, 'a17', 'EC'),
(10, '052023', NULL, NULL, NULL, 'b13', 'EC');

-- --------------------------------------------------------

--
-- Structure de la table `fraisforfait`
--

CREATE TABLE `fraisforfait` (
  `idFraisForfait` varchar(3) NOT NULL,
  `libelleFraisForfait` varchar(20) NOT NULL,
  `montantFraisForfait` decimal(5,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Déchargement des données de la table `fraisforfait`
--

INSERT INTO `fraisforfait` (`idFraisForfait`, `libelleFraisForfait`, `montantFraisForfait`) VALUES
('KM', 'Kilométrage', 5.50),
('NUI', 'Nuitée', 80.00),
('RM', 'Repas midi', 29.00);

-- --------------------------------------------------------

--
-- Structure de la table `lignefraisforfait`
--

CREATE TABLE `lignefraisforfait` (
  `idFraisForfait` varchar(3) NOT NULL,
  `idFicheFrais` int NOT NULL,
  `quantite` smallint NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Déchargement des données de la table `lignefraisforfait`
--

INSERT INTO `lignefraisforfait` (`idFraisForfait`, `idFicheFrais`, `quantite`) VALUES
('KM', 1, 1800),
('KM', 2, 1500),
('KM', 3, 125),
('KM', 4, 0),
('KM', 5, 15),
('KM', 6, 10),
('KM', 7, 40),
('KM', 8, 9),
('KM', 9, 5),
('KM', 10, 0),
('NUI', 1, 2),
('NUI', 2, 1),
('NUI', 3, 5),
('NUI', 4, 0),
('NUI', 5, 8),
('NUI', 6, 6),
('NUI', 7, 2),
('NUI', 8, 10),
('NUI', 9, 11),
('NUI', 10, 0),
('RM', 1, 1),
('RM', 2, 3),
('RM', 3, 5),
('RM', 4, 0),
('RM', 5, 10),
('RM', 6, 15),
('RM', 7, 5),
('RM', 8, 5),
('RM', 9, 3),
('RM', 10, 0);

-- --------------------------------------------------------

--
-- Structure de la table `lignefraishorsforfait`
--

CREATE TABLE `lignefraishorsforfait` (
  `idFicheFrais` int NOT NULL,
  `idLigneFraisHorsForfait` int NOT NULL,
  `date` int NOT NULL,
  `montant` decimal(10,2) NOT NULL,
  `libelle` varchar(100) NOT NULL,
  `justificatif` tinyint(1) NOT NULL DEFAULT '0',
  `nomJustificatif` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Déchargement des données de la table `lignefraishorsforfait`
--

INSERT INTO `lignefraishorsforfait` (`idFicheFrais`, `idLigneFraisHorsForfait`, `date`, `montant`, `libelle`, `justificatif`, `nomJustificatif`) VALUES
(1, 9, 1431388800, 2500.00, 'Billet d\'avion', 0, NULL),
(1, 10, 1460332800, 54.00, 'Fleur', 0, NULL),
(9, 11, 1652313600, 115.00, 'avion', 0, NULL),
(9, 12, 1652313600, 150.00, 'Train', 0, NULL),
(9, 13, 1652313600, 150.00, 'Fusée', 1, 'photoDeLaFusee'),
(10, 14, 1652313600, 115.00, 'avion', 0, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

CREATE TABLE `utilisateur` (
  `idUser` varchar(4) NOT NULL,
  `nom` varchar(30) NOT NULL,
  `prenom` varchar(30) NOT NULL,
  `login` varchar(20) NOT NULL,
  `mdp` varchar(20) NOT NULL,
  `adresse` varchar(30) NOT NULL,
  `ville` varchar(30) NOT NULL,
  `cp` varchar(5) NOT NULL,
  `dateEmbauche` date NOT NULL,
  `profil` enum('Visiteur','Comptable') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;

--
-- Déchargement des données de la table `utilisateur`
--

INSERT INTO `utilisateur` (`idUser`, `nom`, `prenom`, `login`, `mdp`, `adresse`, `ville`, `cp`, `dateEmbauche`, `profil`) VALUES
('a111', 'Zed', 'Arnaud', 'azed', 'Azerty1', '11 rue de la grotte enchantée', 'Paris', '75005', '2010-11-11', 'Comptable'),
('a131', 'Villechalane', 'Louis', 'lvillachane', 'jux7g', '8 rue des Charmes', 'Cahors', '46000', '2005-12-21', 'Visiteur'),
('a17', 'Andre', 'David', 'dandre', 'oppg5', '1 rue Petit', 'Lalbenque', '46200', '1998-11-23', 'Visiteur'),
('a55', 'Bedos', 'Christian', 'cbedos', 'gmhxd', '1 rue Peranud', 'Montcuq', '46250', '1995-01-12', 'Visiteur'),
('a93', 'Tusseau', 'Louis', 'ltusseau', 'ktp3s', '22 rue des Ternes', 'Gramat', '46123', '2000-05-01', 'Visiteur'),
('b13', 'Bentot', 'Pascal', 'pbentot', 'doyw1', '11 allée des Cerises', 'Bessines', '46512', '1992-07-09', 'Visiteur'),
('b16', 'Bioret', 'Luc', 'lbioret', 'hrjfs', '1 Avenue gambetta', 'Cahors', '46000', '1998-05-11', 'Visiteur'),
('b19', 'Bunisset', 'Francis', 'fbunisset', '4vbnd', '10 rue des Perles', 'Montreuil', '93100', '1987-10-21', 'Visiteur'),
('b25', 'Bunisset', 'Denise', 'dbunisset', 's1y1r', '23 rue Manin', 'paris', '75019', '2010-12-05', 'Visiteur'),
('b28', 'Cacheux', 'Bernard', 'bcacheux', 'uf7r3', '114 rue Blanche', 'Paris', '75017', '2009-11-12', 'Visiteur'),
('b34', 'Cadic', 'Eric', 'ecadic', '6u8dc', '123 avenue de la République', 'Paris', '75011', '2008-09-23', 'Visiteur'),
('b4', 'Charoze', 'Catherine', 'ccharoze', 'u817o', '100 rue Petit', 'Paris', '75019', '2005-11-12', 'Visiteur'),
('b50', 'Clepkens', 'Christophe', 'cclepkens', 'bw1us', '12 allée des Anges', 'Romainville', '93230', '2003-08-11', 'Visiteur'),
('b59', 'Cottin', 'Vincenne', 'vcottin', '2hoh9', '36 rue Des Roches', 'Monteuil', '93100', '2001-11-18', 'Visiteur'),
('c14', 'Daburon', 'François', 'fdaburon', '7oqpv', '13 rue de Chanzy', 'Créteil', '94000', '2002-02-11', 'Visiteur'),
('c3', 'De', 'Philippe', 'pde', 'gk9kx', '13 rue Barthes', 'Créteil', '94000', '2010-12-14', 'Visiteur'),
('c54', 'Debelle', 'Michel', 'mdebelle', 'od5rt', '181 avenue Barbusse', 'Rosny', '93210', '2006-11-23', 'Visiteur'),
('d13', 'Debelle', 'Jeanne', 'jdebelle', 'nvwqq', '134 allée des Joncs', 'Nantes', '44000', '2000-05-11', 'Visiteur'),
('d51', 'Debroise', 'Michel', 'mdebroise', 'sghkb', '2 Bld Jourdain', 'Nantes', '44000', '2001-04-17', 'Visiteur'),
('e22', 'Desmarquest', 'Nathalie', 'ndesmarquest', 'f1fob', '14 Place d Arc', 'Orléans', '45000', '2005-11-12', 'Visiteur'),
('e24', 'Desnost', 'Pierre', 'pdesnost', '4k2o5', '16 avenue des Cèdres', 'Guéret', '23200', '2001-02-05', 'Visiteur'),
('e39', 'Dudouit', 'Frédéric', 'fdudouit', '44im8', '18 rue de l église', 'GrandBourg', '23120', '2000-08-01', 'Visiteur'),
('e49', 'Duncombe', 'Claude', 'cduncombe', 'qf77j', '19 rue de la tour', 'La souteraine', '23100', '1987-10-10', 'Visiteur'),
('e5', 'Enault-Pascreau', 'Céline', 'cenault', 'y2qdu', '25 place de la gare', 'Gueret', '23200', '1995-09-01', 'Visiteur'),
('e52', 'Eynde', 'Valérie', 'veynde', 'i7sn3', '3 Grand Place', 'Marseille', '13015', '1999-11-01', 'Visiteur'),
('f21', 'Finck', 'Jacques', 'jfinck', 'mpb3t', '10 avenue du Prado', 'Marseille', '13002', '2001-11-10', 'Visiteur'),
('f39', 'Frémont', 'Fernande', 'ffremont', 'xs5tq', '4 route de la mer', 'Allauh', '13012', '1998-10-01', 'Visiteur'),
('f4', 'Gest', 'Alain', 'agest', 'dywvt', '30 avenue de la mer', 'Berre', '13025', '1985-11-01', 'Visiteur');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `etat`
--
ALTER TABLE `etat`
  ADD PRIMARY KEY (`idEtat`);

--
-- Index pour la table `fichefrais`
--
ALTER TABLE `fichefrais`
  ADD PRIMARY KEY (`idFicheFrais`),
  ADD UNIQUE KEY `u_ficheFraisMois` (`idFicheFrais`,`moisAnnee`),
  ADD KEY `fk_visiteur` (`idVisiteur`),
  ADD KEY `fk_etat` (`idEtat`);

--
-- Index pour la table `fraisforfait`
--
ALTER TABLE `fraisforfait`
  ADD PRIMARY KEY (`idFraisForfait`);

--
-- Index pour la table `lignefraisforfait`
--
ALTER TABLE `lignefraisforfait`
  ADD PRIMARY KEY (`idFraisForfait`,`idFicheFrais`),
  ADD KEY `fk_ficheFrais` (`idFicheFrais`);

--
-- Index pour la table `lignefraishorsforfait`
--
ALTER TABLE `lignefraishorsforfait`
  ADD PRIMARY KEY (`idLigneFraisHorsForfait`),
  ADD KEY `idFicheFrais` (`idFicheFrais`);

--
-- Index pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD PRIMARY KEY (`idUser`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `fichefrais`
--
ALTER TABLE `fichefrais`
  MODIFY `idFicheFrais` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT pour la table `lignefraishorsforfait`
--
ALTER TABLE `lignefraishorsforfait`
  MODIFY `idLigneFraisHorsForfait` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `fichefrais`
--
ALTER TABLE `fichefrais`
  ADD CONSTRAINT `fk_etat` FOREIGN KEY (`idEtat`) REFERENCES `etat` (`idEtat`),
  ADD CONSTRAINT `fk_visiteur` FOREIGN KEY (`idVisiteur`) REFERENCES `utilisateur` (`idUser`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
