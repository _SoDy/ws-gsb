<?php
$serviceContainer = \Propel\Runtime\Propel::getServiceContainer();
$serviceContainer->checkVersion(2);
$serviceContainer->setAdapterClass('ws-gsb-connection', 'mysql');
$manager = new \Propel\Runtime\Connection\ConnectionManagerSingle('ws-gsb-connection');
$manager->setConfiguration(array (
  'classname' => 'Propel\\Runtime\\Connection\\ConnectionWrapper',
  'dsn' => 'mysql:host=service_mysql;dbname=gsb',
  'user' => 'root',
  'password' => 'Azerty1',
  'attributes' =>
  array (
    'ATTR_EMULATE_PREPARES' => false,
    'ATTR_TIMEOUT' => 30,
  ),
  'model_paths' =>
  array (
    0 => 'src',
    1 => 'vendor',
  ),
));
$serviceContainer->setConnectionManager($manager);
$serviceContainer->setDefaultDatasource('ws-gsb-connection');
require_once __DIR__ . '/../script/generated-conf/loadDatabase.php';
