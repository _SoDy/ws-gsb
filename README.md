
# Application GSB
    # Web Service

## Application de gestion des notes de frais
`v1.12.0`

Documentation : ws-gsb.com/documentation

## INSTALLATION DE L'APPLICATION SOUS LINUX 
### L'installation est réalisée depuis l'invite de commande (terminal)

>Téléchargement du code source 

    cd /var/www/html  
    git clone git clone git@bitbucket.org:_SoDy/ws-gsb.git ws-gsb.com

## INSTALLATION DE LA BASE DE DONNÉES

La base de données de l'application doit être installé à partir du fichier 
nommé `gsb.sql` présent dans le répertoire `script` de l'application


